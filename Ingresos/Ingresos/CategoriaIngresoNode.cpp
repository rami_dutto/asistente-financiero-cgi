//
//  CategoriaIngresoNode.cpp
//  ingresos
//
//  Created by Ramiro on 24/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "CategoriaIngresoNode.hpp"

CategoriaIngresoNode::CategoriaIngresoNode()
{
    this->categoriaIngreso = NULL;
    this->next = NULL;
}

CategoriaIngresoNode::CategoriaIngresoNode(CategoriaIngreso* categoriaIngreso)
{
    this->setCategoriaIngreso(categoriaIngreso);
    this->next = NULL;
}

CategoriaIngresoNode::~CategoriaIngresoNode()
{
    // Dtor
}

void CategoriaIngresoNode::setCategoriaIngreso(CategoriaIngreso* categoriaIngreso)
{
    this->categoriaIngreso = categoriaIngreso;
    this->next = NULL;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función getElement() devuelve un puntero a un objeto de tipo ingreso*/
CategoriaIngreso* CategoriaIngresoNode::getCategoriaIngreso()
{
    return this->categoriaIngreso;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void CategoriaIngresoNode::setNext(CategoriaIngresoNode* next)
{
    this->next = next;
}

CategoriaIngresoNode* CategoriaIngresoNode::getNext()
{
    return this->next;
}

void CategoriaIngresoNode::show()
{
    cout << categoriaIngreso->toString();
}
