//
//  ingreso.cpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "ingreso.hpp"

using namespace std;

Ingreso::Ingreso()
{
    id = 0;
    this->monto = 0;
    this->detalle = "";
    this->categoria = 0;
    this->fecha = "";
    
}

Ingreso::Ingreso(sql::ResultSet* res)
{
    this->fillObject(res);
}


void Ingreso::fillObject(sql::ResultSet* rs)
{
    this->setId(rs->getInt("id"));
    this->setCategoria(rs->getInt("id_categoria_ingreso"));
    this->setFecha(rs->getString("fecha"));
    this->setMonto(rs->getInt("monto"));
    this->setDetalle(rs->getString("detalle"));
    
}

/* ID. */
void Ingreso::setId(int id)
{
    this->id = id;
}

int Ingreso::getId()
{
    return this->id;
}

/* NOMBRE. */
void Ingreso::setCategoria(int Ingreso)
{
    this->categoria = Ingreso;
}

int Ingreso::getCategoria()
{
    return this->categoria;
}

/* Fecha. */
string Ingreso::getFecha()
{
    return this->fecha;
}

void Ingreso::setFecha(string date)
{
    this->fecha = date;
}

/* Monto.*/
int Ingreso::getMonto()
{
    return this->monto;
}

void Ingreso::setMonto(float total)
{
    this->monto = total;
}

/* Detalle*/
string Ingreso::getDetalle()
{
    return this->detalle;
}

void Ingreso::setDetalle(string detalle)
{
    this->detalle = detalle;
}
string Ingreso::toString()
{
    CategoriaIngresoDAO* cid = new CategoriaIngresoDAO();
    CategoriaIngreso* ci;
    Datos* datosChild = new Datos();
    
    datosChild->setActionNoCgi(4);
    datosChild->setIdNoCgi(this->getCategoria());
    
    ci = cid->find(datosChild->getId());
    
    
    stringstream id;
    stringstream catid;
    stringstream monto;
    stringstream detalle;
    stringstream fecha;
    stringstream urlEdit;
    stringstream urlDel;
    
    id << setfill(' ') << left << setw(3) << this->getId();
    catid << setfill(' ') << left << setw(5) << ci->getNombre();
    monto << setfill(' ') << left << setw(5) << this->getMonto();
    detalle << setfill(' ') << left << setw(25) << this->getDetalle();
    fecha << setfill(' ') << left << setw(20) << this->getFecha();
    urlEdit << "./ingresos.cgi?action=1&id=" << this->getId();
    urlDel << "./ingresos.cgi?action=2&id=" << this->getId();
    
    return "<tr> <td>" + id.str() + "</td> <td>" + catid.str() + "</td> \
           <td>" + "$ " + monto.str() + "</td><td>" + detalle.str() + "</td><td>" + fecha.str() + "</td><td> \
    <a href='" + urlEdit.str() + "'><img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></a></td><td> \
    <a href='" + urlDel.str() + "'><img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></a></td></tr>";
    
    /*
           <form method='get' action='./ingresos.cgi?1&valor=" + id.str() + "'> <input type='submit' value='Modificar'/>\
            <img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></form></td>\
           <td><form method='get' action='./ingresos.cgi?2&valor=" + id.str() + "'> <input type='submit' value='Eliminar'/>\
           <img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></form></td></tr>";*/
    
    /* */
}
