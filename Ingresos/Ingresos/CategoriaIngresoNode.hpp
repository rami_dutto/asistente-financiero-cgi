//
//  CategoriaIngresoNode.hpp
//  ingresos
//
//  Created by Ramiro on 24/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CategoriaIngresoNode_hpp
#define CategoriaIngresoNode_hpp

#include <stdio.h>

#include "CategoriaIngreso.hpp"

class CategoriaIngresoNode
{
public:
    CategoriaIngresoNode();
    CategoriaIngresoNode(CategoriaIngreso*);
    virtual ~CategoriaIngresoNode();
    void setCategoriaIngreso(CategoriaIngreso*);
    CategoriaIngreso* getCategoriaIngreso();
    void setNext(CategoriaIngresoNode*);
    CategoriaIngresoNode* getNext();
    void show();
protected:
    
private:
    CategoriaIngreso* categoriaIngreso;
    CategoriaIngresoNode* next;
};

#endif /* CategoriaIngresoNode_hpp */
