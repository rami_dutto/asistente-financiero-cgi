//
//  CategoriaIngresoQueue.hpp
//  ingresos
//
//  Created by Ramiro on 24/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CATEGORIAINGRESOQUEUE_HPP
#define CATEGORIAINGRESOQUEUE_HPP

#include <stdio.h>
#include <iostream>

#include "CategoriaIngresoNode.hpp"

class CategoriaIngresoQueue
{
public:
    CategoriaIngresoNode* qstart;
    CategoriaIngresoNode* qend;
    
    CategoriaIngresoQueue();
    ~CategoriaIngresoQueue();
    void setQStart(CategoriaIngresoNode*);
    CategoriaIngresoNode* getQStart();
    void setQEnd(CategoriaIngresoNode*);
    CategoriaIngresoNode* getQEnd();
    void qstore(CategoriaIngreso*);
    CategoriaIngreso* qretrieve();
    void free();
    void show();
    
protected:
private:
};

#endif /* CATEGORIAINGRESOQUEUE_HPP */
