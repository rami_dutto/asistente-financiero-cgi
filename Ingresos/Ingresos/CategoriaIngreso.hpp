//
//  categoriaIngreso.hpp
//  Asistente Financiero
//
//  Created by rama on 29/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef CATEGORIAINGRESO_HPP
#define CATEGORIAINGRESO_HPP

#include <stdio.h>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <string>

using namespace std;

class CategoriaIngreso {
public:
    CategoriaIngreso();
    CategoriaIngreso(sql::ResultSet*);
    ~CategoriaIngreso();
    
    void setId(int);
    int getId();
    
    void setNombre(string);
    string getNombre();
    
    void fillObject(sql::ResultSet*);
    string toString();
    
private:
    int id;
    string nombre;
};
#endif /* categoriaIngreso_hpp */
