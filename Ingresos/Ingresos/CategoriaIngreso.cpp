//
//  categoriaIngreso.cpp
//  Asistente Financiero
//
//  Created by rama on 29/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "categoriaIngreso.hpp"

CategoriaIngreso::CategoriaIngreso() {
    this->id = 0;
    this->nombre = "";
}
CategoriaIngreso::CategoriaIngreso(sql::ResultSet* res) {

        this->fillObject(res);
}
void CategoriaIngreso::fillObject(sql::ResultSet* rs)
{
    this->setId(rs->getInt("id"));
    this->setNombre(rs->getString("nombre"));
    
}
CategoriaIngreso::~CategoriaIngreso() {
    
}

void CategoriaIngreso::setId(int a) {
    this->id = a;
}
int CategoriaIngreso::getId() {
    return this->id;
}

void CategoriaIngreso::setNombre(string a) {
    this->nombre = a;
}
string CategoriaIngreso::getNombre() {
    return this->nombre;
}

string CategoriaIngreso::toString()
{
    stringstream id;
    stringstream nombre;
    stringstream urlEdit;
    stringstream urlDel;
    
    id << setfill(' ') << left << setw(3) << this->getId();
    nombre << setfill(' ') << left << setw(5) << this->getNombre();
    urlEdit << "./ingresos.cgi?action=1&id=" << this->getId();
    urlDel << "./ingresos.cgi?action=2&id=" << this->getId();
    
    return "<tr> <td>" + id.str() + "</td> <td>" + nombre.str() + "</td><td> \
    <a href='" + urlEdit.str() + "'><img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></a></td><td> \
    <a href='" + urlDel.str() + "'><img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></a></td></tr>";
    
    /*
     <form method='get' action='./ingresos.cgi?1&valor=" + id.str() + "'> <input type='submit' value='Modificar'/>\
     <img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></form></td>\
     <td><form method='get' action='./ingresos.cgi?2&valor=" + id.str() + "'> <input type='submit' value='Eliminar'/>\
     <img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></form></td></tr>";*/
    
    /* */
}
