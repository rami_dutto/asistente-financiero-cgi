//
//  dbconnection.cpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "MyConnection.hpp"

using namespace std;
using namespace sql;

MyConnection* MyConnection::pInstance = NULL;

MyConnection* MyConnection::instance()
{
    if (pInstance == NULL)
    {
        pInstance = new MyConnection;
    }
    return pInstance;
}

MyConnection::MyConnection()
{
    this->connection = NULL;
}

MyConnection::~MyConnection()
{
    this->disconnect();
}

void MyConnection::disconnect()
{
    delete this->connection;
}

void MyConnection::connect()
{
    string server = "tcp://localhost:3306";
    string user = "rama";
    string password = "1234";
    string database = "asistentefinanciero";
    
    /* Connect to database */
    Driver* driver = get_driver_instance();
    this->connection = driver->connect(server, user, password);
    connection->setSchema(database);
}

Connection* MyConnection::getConnection()
{
    return this->connection;
}

ResultSet* MyConnection::query(string stringSQL)
{
    if (!this->connection)
        this->connect();
    
    return this->connection->createStatement()->executeQuery(stringSQL);
}

void MyConnection::execute(string stringSQL)
{
    if (!this->connection)
        this->connect();
    
    this->connection->createStatement()->executeUpdate(stringSQL);
}
