//
//  menuViewer.hpp
//  Asistente Financiero
//
//  Created by Ramiro on 3/11/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef MENUVIEWER_HPP
#define MENUVIEWER_HPP

#include <stdio.h>


class MenuViewer
{
public:
    MenuViewer();
    virtual ~MenuViewer();
    int menu();
protected:
private:
    //int optionMC;
};

#endif /* menuViewer_hpp */
