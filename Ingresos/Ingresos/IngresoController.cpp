//
//  ingresoController.cpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "ingresoController.hpp"

IngresoController::IngresoController()
{
    //ctor
}

IngresoController::~IngresoController()
{
    //dtor
}

void IngresoController::abm(Datos* data)
{
    IngresoViewer* iv;
    iv = new IngresoViewer();
    
    bool salir = false;
    
    while (!salir)
    {
        switch (data->getAction())
        {
            case 0:
                iv->menu(data);
                data->setActionNoCgi(6);
                break;
            case 1:
                //modificar(idIc);
                if (data->getConfirm() == 1) {
                    modificar(data);
                    data->setActionNoCgi(0);
                }
                iv->menu(data);
                data->setActionNoCgi(6);
                break;
            case 2:
                eliminar(data);
                iv->menu(data);
                data->setActionNoCgi(6);
                break;
            case 3:
                if (data->getConfirm() == 1) {
                    agregar(data);
                    data->setActionNoCgi(0);
                }
                iv->menu(data);
                data->setActionNoCgi(6);
                break;
            case 6:
                salir = true;
                break;
        }
    }
    delete iv;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función listar crea un objeto de tipo IngresoViewer y llama a la fnción listar()
 * de dicho  objeto a la cual se le pasa como argumento la función collection() pertene-
 * ciente al objeto de tipo IngresoDAO. Ver como funciona listar() de la clase IngresoViewer,
 * ver como funciona collection() de la clase IngresoDAO*/
void IngresoController::listar(Datos* datos)
{
    (new IngresoViewer())->listar((new IngresoDAO())->collection());
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void IngresoController::buscar(int idIc, Datos* datos)
{
    IngresoViewer* iv = new IngresoViewer();
    iv->mostrar((new IngresoDAO())->find(iv->buscar(idIc)), datos);
}

void IngresoController::agregar(Datos* datos)
{
    Ingreso* cl = new Ingreso();
    cargar(cl, datos);
    (new IngresoDAO())->add(cl);
}

int IngresoController::eliminar(Datos* datos)
{
    Ingreso* cl = new Ingreso();
    IngresoViewer* cv = new IngresoViewer();
    
    cl = (new IngresoDAO())->find(cv->buscar(datos->getId()));
    (new IngresoDAO())->del(cl);
    
    return 6;
}

void IngresoController::modificar(Datos* datos)
{
    
    Ingreso* cl = new Ingreso();
    
    cl = (new IngresoDAO())->find(datos->getId());
    this->cargar(cl, datos);
    (new IngresoDAO())->update(cl);
}

void IngresoController::cargar(Ingreso *cl, Datos * datos)
{
    cl->setId(datos->getId());
    cl->setDetalle(datos->getDescripcion());
    cl->setMonto(datos->getMonto());
    cl->setFecha(datos->getFecha());
    cl->setCategoria(datos->getCatID());
}


