//
//  CategoriaIngresoController.cpp
//  ingresos
//
//  Created by Ramiro on 24/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "CategoriaIngresoController.hpp"

CategoriaIngresoController::CategoriaIngresoController()
{
    //ctor
}

CategoriaIngresoController::~CategoriaIngresoController()
{
    //dtor
}

void CategoriaIngresoController::abm(Datos* data)
{
    CategoriaIngresoViewer* civ;
    civ = new CategoriaIngresoViewer();
    
    bool salir = false;
    
    while (!salir)
    {
        switch (data->getAction())
        {
            case 0:
                civ->menu(data);
                data->setActionNoCgi(6);
                break;
            case 1:
                //modificar(idIc);
                if (data->getConfirm() == 1) {
                    modificar(data);
                    data->setActionNoCgi(0);
                }
                civ->menu(data);
                data->setActionNoCgi(6);
                break;
            case 2:
                eliminar(data);
                civ->menu(data);
                data->setActionNoCgi(6);
                break;
            case 3:
                if (data->getConfirm() == 1) {
                    agregar(data);
                    data->setActionNoCgi(0);
                }
                civ->menu(data);
                data->setActionNoCgi(6);
                break;
            case 4:
                buscar(data->getAction(), data);
            case 6:
                salir = true;
                break;
        }
    }
    delete civ;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función listar crea un objeto de tipo IngresoViewer y llama a la fnción listar()
 * de dicho  objeto a la cual se le pasa como argumento la función collection() pertene-
 * ciente al objeto de tipo IngresoDAO. Ver como funciona listar() de la clase IngresoViewer,
 * ver como funciona collection() de la clase IngresoDAO*/
void CategoriaIngresoController::listar()
{
    (new CategoriaIngresoViewer())->listar((new CategoriaIngresoDAO())->collection());
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void CategoriaIngresoController::buscar(int idIc, Datos* datos)
{
    CategoriaIngresoViewer* civ = new CategoriaIngresoViewer();
    civ->mostrar((new CategoriaIngresoDAO())->find(civ->buscar(idIc)), datos);
}

void CategoriaIngresoController::agregar(Datos* datos)
{
    CategoriaIngreso* cl = new CategoriaIngreso();
    cargar(cl, datos);
    (new CategoriaIngresoDAO())->add(cl);
}

int CategoriaIngresoController::eliminar(Datos* datos)
{
    CategoriaIngreso* cl = new CategoriaIngreso();
    CategoriaIngresoViewer* civ = new CategoriaIngresoViewer();
    
    cl = (new CategoriaIngresoDAO())->find(civ->buscar(datos->getId()));
    (new CategoriaIngresoDAO())->del(cl);
    
    return 6;
}

void CategoriaIngresoController::modificar(Datos* datos)
{
    
    CategoriaIngreso* cl = new CategoriaIngreso();
    
    cl = (new CategoriaIngresoDAO())->find(datos->getId());
    this->cargar(cl, datos);
    (new CategoriaIngresoDAO())->update(cl);
}

void CategoriaIngresoController::cargar(CategoriaIngreso *cl, Datos * datos)
{
    cl->setId(datos->getId());
    cl->setNombre(datos->getNombre());
}
