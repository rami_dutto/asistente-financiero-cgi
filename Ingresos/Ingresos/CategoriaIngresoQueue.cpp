//
//  CategoriaIngresoQueue.cpp
//  ingresos
//
//  Created by Ramiro on 24/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "CategoriaIngresoQueue.hpp"

using namespace std;

CategoriaIngresoQueue::CategoriaIngresoQueue()
{
    qstart = qend = NULL;
}

CategoriaIngresoQueue::~CategoriaIngresoQueue()
{
    
}

void CategoriaIngresoQueue::setQStart(CategoriaIngresoNode* qstart)
{
    this->qstart = qstart;
}

CategoriaIngresoNode* CategoriaIngresoQueue::getQStart()
{
    return this->qstart;
}

void CategoriaIngresoQueue::setQEnd(CategoriaIngresoNode* qend)
{
    this->qend = qend;
}

CategoriaIngresoNode* CategoriaIngresoQueue::getQEnd()
{
    return this->qend;
}

void CategoriaIngresoQueue::qstore(CategoriaIngreso* element)
{
    CategoriaIngresoNode* newNode = new CategoriaIngresoNode();
    
    newNode->setCategoriaIngreso(element);
    newNode->setNext(NULL);
    
    if (qstart == NULL)
    {
        qstart = qend = newNode;
        return;
    }
    
    qend->setNext(newNode); //Hace que el último apunte al nuevo último
    
    qend = newNode; // Hace que el último sea el nuevo
}

CategoriaIngreso* CategoriaIngresoQueue::qretrieve()
{
    if (this->qstart == NULL)
        return NULL;
    CategoriaIngreso* aux = this->qstart->getCategoriaIngreso(); //Recupero elemento a devolver
    CategoriaIngresoNode* nx = this->qstart; // Apunto nodo para no perder la referencia
    
    this->qstart = nx->getNext(); // Corro el start al siguiente nodo
    
    if (this->qstart == NULL)
        this->qend = NULL;
    
    delete nx; // Elimino el que era el primer nodo de la cola
    
    return aux; // Devuelvo el elemento recuperado
    
}

void CategoriaIngresoQueue::free()
{
    while (this->qretrieve() != NULL);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */
void CategoriaIngresoQueue::show()
{
    /* qstar es un puntero a un objeto de tipo node y apunta al primer nodo de la cola
     * si qstar equivale a NULL, no apunta a ningun nodo y show() termina, si no, con-
     * tinua
     if (this->qstart == NULL)
     return;
     */
    /* si se sala el 'if' se crea aux que es un puntero a un objeto de tipo node y se
     * apunta al mismo nodo que qstar, es decir; el primer nodo de la lista*/
    CategoriaIngresoNode* aux = this->qstart;
    
    /* Mientras aux sea distinti de NULL se ejecuta el while */
    while (aux)
    {
        aux->show();
        /* Al ser aux un puntero a node, este puede invocar los metodos de node, asi que
         * invoca el metodo getElement() de node */
        cout << endl;
        /* Hago que aux apunte a la dirección de memoria del siguiente objeto node*/
        aux = aux->getNext();
    }
    
    cout << endl;
    
}
