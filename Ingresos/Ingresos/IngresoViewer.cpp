//
//  ingresoViewer.cpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//
#include "ingresoViewer.hpp"

/* CREACION DEL CONSTRUCTOR */
IngresoViewer::IngresoViewer()
{
    //ctor
}

/* CREACION DEL DESTRUCTOR */
IngresoViewer::~IngresoViewer()
{
    //dtor
}

int IngresoViewer::menu(Datos* datos)
{
    IngresoViewer* iv = new IngresoViewer();
    
    cout << "Content-Type: text/html\n\r\n\r"
    <<  "<!DOCTYPE html>\n"
    <<  "<html >\n"
    <<  "<head>\n"
    <<  "  <meta charset='UTF-8'>\n"
    <<  "  <title>maquetado html5</title>\n"
    <<  "  <title>Asistente Financiero</title>\n"
    <<  "  <link type='text/css' rel='stylesheet' href='../CSS/style.css'>\n"
    <<  "  <script type='text/javascript' href='../JS/jscript.js'></script>\n"
    <<  "  <link href='https://fonts.googleapis.com/css?family=Sonsie+One' rel='stylesheet'> \n"
    <<  "  <link href='https://fonts.googleapis.com/css?family=Khand' rel='stylesheet'> \n"
    <<  "  <link href='../bootstrap/css/bootstrap.min.css' rel='stylesheet'>\n"
    <<  "  <link href='../bootstrap/css/bootstrap-theme.min.css' rel='stylesheet'>\n"
    <<  "  <link rel='stylesheet' href='css/style.css'>\n"
    <<  "</head>\n"
    <<  "\n"
    <<  "<body>\n"
    <<  "  <div class='content'>\n"
    <<  "   <div class='row'>"
    <<  "    <header>\n"
    <<  "      <img src='https://cdn2.iconfinder.com/data/icons/money-operations/512/money_box-512.png' alt='logo'>\n"
    <<  "      <h1>Big $avings</h1>\n"
    <<  "      <nav>\n"
    <<  "        <ul>\n"
    <<  "          <li><a href=''>Ingresos</a></li>\n"
    <<  "          <li><a href=''>Egresos</a></li>\n"
    <<  "          <li><a href=''>Ahorros</a></li>\n"
    <<  "          <li><a href=''>Configuración</a></li>\n"
    <<  "        </ul>\n"
    <<  "      </nav>\n"
    <<  "    </header>\n"
    <<  "    <section>\n"
    <<  "       <img class='section-bg' src='http://previews.123rf.com/images/ladiseno/ladiseno1308/ladiseno130800226/21525677-big-saving-label-with-a-pig-on-light-brown-background-Stock-Vector.jpg'   alt='fondo'>\n";
    
    if ((datos->getAction() == 0) || (datos->getAction() == 2)) {
        iv->listar((new IngresoDAO())->collection());
        
    } else if ((datos->getAction() == 1) || (datos->getAction() == 3)) {
        iv->mostrar((new IngresoDAO())->find(iv->buscar(datos->getId())), datos);
    }
    cout <<  "    </section>\n"
    <<  "    <aside>\n"
    <<  "      <h2><span style='color:gray;'>Hora actual en</span><br/>Mendoza, Argentina  </h2> \n"
    <<  "      <iframe src='http://www.zeitverschiebung.net/clock-widget-iframe-v2?langua  ge=es&timezone=America%2FArgentina%2FMendoza' width='100%' height='150' frameborder='0'   se  amless></iframe> <small style='color:gray;'></small>\n"
    <<  "    </aside>\n"
    <<  "  </div>\n"
    <<  "  </div>\n"
    <<  "</body>\n"
    <<  "</html>\n";
    
    return 6;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función listar tiene como parametro un puntero a un objeto queue(Cola) al 
 * al invocar a la funcion listar se le pasa como argumento un puntero a un bojeto queue
 * y se invoca la función show() de dicho objeto. En el archivo queue.cpp se detalla el 
 * funcionamiento de la función show()*/
void IngresoViewer::listar(IngresoQueue* ingresoQueue)
{
    cout <<  "       <h1>Ingresos</h1>\n"
    <<  "<div id='constrainer'>\n"
    <<  "<div class='scrolltable'>\n"
    <<  "<table class'header'><thead><th>ID</th><th>Catergoría ID</th><th>Monto</th><th>Descripción</th><th>Fecha</th><th>Modificar</th><th>Eliminar</th></thead></table>\n"
    <<  "<div class='body'>\n"
    <<  "<table>\n"
    <<  "<tbody>\n";
    ingresoQueue->show();
    cout << "</tbody>\n"
    <<  "</table>\n"
    <<  "</div>\n"
    <<  "<table>\n"
    <<  "<a class='footer' href='./ingresos.cgi?action=3'>\n"
    <<  "<tf>Agregar Ingreso</tf>\n"
    <<  "</a>\n"
    <<  "</table>\n"
    <<  "</div>\n"
    <<  "</div>\n";
    
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/




int IngresoViewer::buscar(int idIv)
{
    int id = idIv;
    
    return id;
}
/* Funcin para mostrar todos los valores de un Ingreso */
void IngresoViewer::mostrar(Ingreso* in, Datos* datos)
{
    string menu;
    string action;
    stringstream id;
    stringstream monto;
    stringstream detalle;
    stringstream fecha;
    
    if (datos->getAction() == 1) {
        id << "value = '" << in->getId() << "'";
        monto << "value = '" << in->getMonto() << "'";
        detalle << "value = '" << in->getDetalle() <<"'";
        fecha << "value = '" << in->getFecha() << "'";
        action = "1";
    } else {
        id << "";
        monto << "";
        detalle << "";
        fecha << "";
        action = "3";
    }
    
    
    cout <<  "<article class='add'>\n"
    <<  "<form name='form1' action='./ingresos.cgi' method='get'>\n"
    <<  "<div class='categoria_add'>"
    <<  "<p>CATEGORIA</p>"
    <<  "Agregar:<a href='./ingresos.cgi?action=6'>Agregar categoría</a>\n"
    <<  "<br/>\n"
    <<  "Seleccionar: <select name='cat_id'>\n"
    <<  "<option>1</option>\n"
    <<  "</select>\n"
    <<  "</div>\n"
    <<  "<p>"
    <<  "<input type='text' name='menu' value='1' style='display: none'>\n"
    <<  "<input type='text' name='action' value='" + action + "' style='display: none'>\n"
    <<  "<input type='text' name='confirm' value='1' style='display: none'>\n"
    <<  "<input type='text' name='id' " + id.str() + " style='display: none'>\n"
    <<  "Descripción: <input type='text' name='des' " + detalle.str() + " required />\n"
    <<  "<br />\n"
    <<  "Monto: <input type='text' name='monto' min='0' max='999999' " + monto.str() + " required />\n"
    <<  "<br />"
    <<  "Fecha: <input type='date' " + fecha.str() + " name='fecha' />\n"
    <<  "<br />\n"
    <<  "<input type='submit' value='Confirmar' /><input type='button' value='Cancelar' onClick=\"window.location='./ingresos.cgi'\">\n"
    <<  "</p>\n"
    <<  "</form>\n"
    <<  "       </article>\n";
}



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

