//
//  main.cpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//
//#include <stdio.h>

#include <stdio.h>
#include <iostream>

/*#include <cgicc/CgiDefs.h>
#include <cgicc/Cgicc.h>
#include <cgicc/HTTPHTMLHeader.h>
#include <cgicc/HTMLClasses.h>*/

using namespace std;
//using namespace cgicc;
    
#include "IngresoController.hpp"
#include "Ingreso.hpp"
#include "Datos.hpp"

const string OPT[ 24 ] = {
    "menu", "action", "confirm", "id",
    "cat_id", "fecha", "monto", "des",
    "nombre"
};


int main (int argc, char* const argv[]) {
    
    Datos* data = new Datos();
    
    data->setMenu(OPT[0]);
    data->setAction(OPT[1]);
    data->setConfirm(OPT[2]);
    data->setId(OPT[3]);
    data->setCatId(OPT[4]);
    data->setFecha(OPT[5]);
    data->setMonto(OPT[6]);
    data->setDescripcion(OPT[7]);
    data->setNombre(OPT[8]);
    
    /*int action;
    int confirm;
    
    cout << "Accion: ";
    cin >> action;
    
    cout << "\nConfirm: ";
    cin >> confirm;
    
    data->setActionNoCgi(action);
    data->setConfirmNoCgi(confirm);*/
    
    
    /*cout << "Content-Type: text/html\n\r\n\r";
    cout << "choice: " << data->getChoice() << endl << "<br />"
    <<  "id: " << data->getId() << endl << "<br />"
    <<  "confirm: " << data->getConfirm() << endl << "<br />"
    <<  "cat_id: " << data->getCatID() << endl << "<br />"
    <<  "fecha: " << data->getFecha() << endl << "<br />"
    <<  "monto: " << data->getMonto() << endl << "<br />"
    <<  "des: " << data->getDescripcion() << endl << "<br />"; */
    
    IngresoController* ic = new IngresoController();
    ic->abm(data);
    delete ic;
    
    return 0;
}
