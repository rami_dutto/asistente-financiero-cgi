//
//  ingresoController.hpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef INGRESOCONTROLLER_HPP
#define INGRESOCONTROLLER_HPP

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "IngresoViewer.hpp"
#include "CategoriaIngresoViewer.hpp"
#include "IngresoDAO.hpp"
#include "CategoriaIngresoDAO.hpp"
#include "Datos.hpp"
#include "Ingreso.hpp"

class IngresoController
{
public:
    IngresoController();
    virtual ~IngresoController();
    void abm(Datos*);
protected:
private:
    void listar(Datos*);
    void buscar(int, Datos*);
    void agregar(Datos*);
    void modificar(Datos*);
    int eliminar(Datos*);
    void cargar(Ingreso*, Datos*);
};

#endif /* INGRESOCONTROLLER_HPP */
