//
//  CategoriaIngresoController.hpp
//  ingresos
//
//  Created by Ramiro on 24/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CategoriaIngresoController_hpp
#define CategoriaIngresoController_hpp

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "CategoriaIngresoViewer.hpp"
#include "CategoriaIngresoDAO.hpp"
#include "Datos.hpp"
#include "CategoriaIngreso.hpp"

class CategoriaIngresoController
{
public:
    CategoriaIngresoController();
    virtual ~CategoriaIngresoController();
    void abm(Datos*);
protected:
private:
    void listar();
    void buscar(int, Datos*);
    void agregar(Datos*);
    void modificar(Datos*);
    int eliminar(Datos*);
    void cargar(CategoriaIngreso*, Datos*);
};

#endif /* CategoriaIngresoController_hpp */
