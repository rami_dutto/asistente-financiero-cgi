//
//  menuController.cpp
//  Asistente Financiero
//
//  Created by Ramiro on 3/11/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "MenuController.hpp"
#include "MenuViewer.hpp"

MenuController::MenuController()
{
    // Ctor.
}

MenuController::~MenuController()
{
    // Dtor.
}

void MenuController::menu()
{
    bool loopCtrl = false;
    MenuViewer* mv;
    mv = new MenuViewer;
    
    while (!loopCtrl) {
        switch (mv->menu()) {
            case 1:
                IngresoController* i;
                i = new IngresoController();
                i->abm();
                break;
                
            default:
                loopCtrl = true;
                break;
        }
    }
    
}


