#include "ingresoDAO.hpp"

IngresoDAO::IngresoDAO()
{
    //ctor
}

IngresoDAO::~IngresoDAO()
{
    //dtor
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
IngresoQueue* IngresoDAO::collection()
{
    /* Se crea un puntero queue que apunta a un objeto Queue*/
    IngresoQueue* ingresoQueue = new IngresoQueue();
    /* MyConnection devuelve dotos los registros de la tabla ingreso y los almacena en memoria.
     * res es un puntero que apunta al comienzo de dicha dirección de memoria. */
    sql::ResultSet* res = MyConnection::instance()->query("SELECT * FROM ingreso ORDER BY id");
    
    /* Se van creando objetos de tipo Ingreso a los cuales se los va seteando con los valores
     * contenidos en la memoria, el while finaliza cuando get nex ya no encuentra datos para
     * setear mas objetos de tipo Ingreso. */
    while (res->next())
        ingresoQueue->qstore(new Ingreso(res));
    
    delete res;
    
    return ingresoQueue;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++*/

void IngresoDAO::del(Ingreso* ingreso)
{
    stringstream stringSQL;
    
    stringSQL << "DELETE FROM ingreso WHERE id = "
              << ingreso->getId()
              << ";";
    
    MyConnection::instance()->execute(stringSQL.str());
}

void IngresoDAO::save(Ingreso* Ingreso)
{
    if (exist(Ingreso))
        update(Ingreso);
    else
        add(Ingreso);
}

bool IngresoDAO::exist(Ingreso* Ingreso)
{
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM ingreso WHERE id = "
              << Ingreso->getId()
              << ";";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    return res->next();
}

void IngresoDAO::update(Ingreso* Ingreso)
{
    stringstream stringSQL;
    
    stringSQL << "UPDATE ingreso SET detalle = '"
              << Ingreso->getDetalle()
              << "' WHERE id = "
              << Ingreso->getId()
              << ";";
    
    MyConnection::instance()->execute(stringSQL.str());
}

void IngresoDAO::add(Ingreso* Ingreso)
{
    
    stringstream stringSQL;
    
    stringSQL << "INSERT INTO ingreso (id, id_categoria_ingreso, monto, detalle, fecha) VALUES ("
              << Ingreso->getId() << ","
              << "'" << Ingreso->getCategoria() << "',"
              << Ingreso->getMonto() << ", "
              << "'" << Ingreso->getDetalle() << "', "
              << "'" << Ingreso->getFecha() << "'"
              << ");";
    
    MyConnection::instance()->execute(stringSQL.str());
}

Ingreso* IngresoDAO::find(int id)
{
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM ingreso WHERE id = "
              << id
              << ";";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    if (res->next())
    {
        Ingreso* ingreso = new Ingreso(res);
        delete res;
        return ingreso;
    }
    
    delete res;
    return new Ingreso();
}
