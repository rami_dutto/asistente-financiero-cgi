//
//  ingresoViewer.hpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef INGRESOVIEWER_HPP
#define INGRESOVIEWER_HPP

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "IngresoController.hpp"
#include "ingresoDAO.hpp"
#include "IngresoQueue.hpp"
#include "Datos.hpp"



using namespace std;

class IngresoViewer
{
public:
    IngresoViewer();
    virtual ~IngresoViewer();
    int menu(Datos*);
    void listar(IngresoQueue*);
    int buscar(int);
    void mostrar(Ingreso*, Datos*);
protected:
private:
};

#endif /* ingresoViewer_hpp */
