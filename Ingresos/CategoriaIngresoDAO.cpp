//
//  CategoriaIngresoDAO.cpp
//  ingresos
//
//  Created by Ramiro on 24/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "CategoriaIngresoDAO.hpp"

CategoriaIngresoDAO::CategoriaIngresoDAO()
{
    //ctor
}

CategoriaIngresoDAO::~CategoriaIngresoDAO()
{
    //dtor
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
CategoriaIngresoQueue* CategoriaIngresoDAO::collection()
{
    /* Se crea un puntero queue que aounta a un objeto Queue*/
    CategoriaIngresoQueue* categoriaQueue = new CategoriaIngresoQueue();
    sql::ResultSet* res = MyConnection::instance()->query("SELECT * FROM categoria_de_ingreso ORDER BY id");
    
    while (res->next())
        categoriaQueue->qstore(new CategoriaIngreso(res));
    
    delete res;
    
    return categoriaQueue;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++*/

void CategoriaIngresoDAO::del(CategoriaIngreso* Categoriaingreso)
{
    stringstream stringSQL;
    
    stringSQL << "DELETE FROM categoria_de_ingreso WHERE id = "
    << Categoriaingreso->getId()
    << ";";
    
    MyConnection::instance()->execute(stringSQL.str());
}

void CategoriaIngresoDAO::save(CategoriaIngreso* CategoriaIngreso)
{
    if (exist(CategoriaIngreso))
        update(CategoriaIngreso);
    else
        add(CategoriaIngreso);
}

bool CategoriaIngresoDAO::exist(CategoriaIngreso* CategoriaIngreso)
{
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM categoria_de_ingreso WHERE id = "
    << CategoriaIngreso->getId()
    << ";";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    return res->next();
}

void CategoriaIngresoDAO::update(CategoriaIngreso* CategoriaIngreso)
{
    stringstream stringSQL;
    
    stringSQL << "UPDATE categoria_de_ingreso SET nombre = '"
    << CategoriaIngreso->getNombre()
    << "' WHERE id = "
    << CategoriaIngreso->getId()
    << ";";
    
    MyConnection::instance()->execute(stringSQL.str());
}

void CategoriaIngresoDAO::add(CategoriaIngreso* CategoriaIngreso)
{
    
    stringstream stringSQL;
    
    stringSQL << "INSERT INTO categoria_de_ingreso (id, nombre) VALUES ("
    << CategoriaIngreso->getId() << ", '"
    << CategoriaIngreso->getNombre() << "'"
    << ");";
    
    MyConnection::instance()->execute(stringSQL.str());
}

CategoriaIngreso* CategoriaIngresoDAO::find(int id)
{
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM categoria_de_ingreso WHERE id = "
    << id
    << ";";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    if (res->next())
    {
        CategoriaIngreso* categoriaIngreso = new CategoriaIngreso(res);
        delete res;
        return categoriaIngreso;
    }
    
    delete res;
    return new CategoriaIngreso();
}
