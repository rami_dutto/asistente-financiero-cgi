//
//  CategoriaIngresoDAO.hpp
//  ingresos
//
//  Created by Ramiro on 24/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CATEGORIAINGRESODAO_HPP
#define CATEGORIAINGRESODAO_HPP

#include <stdio.h>
#include <sstream>

#include "CategoriaIngreso.hpp"
#include "CategoriaIngresoQueue.hpp"
#include "MyConnection.hpp"


class CategoriaIngresoDAO
{
public:
    CategoriaIngresoDAO();
    virtual ~CategoriaIngresoDAO();
    
    void add(CategoriaIngreso*);
    
    void update(CategoriaIngreso*);
    
    bool exist(CategoriaIngreso*);
    
    void save(CategoriaIngreso*);
    
    void del(CategoriaIngreso*);
    
    CategoriaIngreso* find(int);
    
    CategoriaIngresoQueue* collection();
    
protected:
private:
};

#endif /* CATEGORIAINGRESODAO_HPP */
