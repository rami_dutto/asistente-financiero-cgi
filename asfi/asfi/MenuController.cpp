//
//  menuController.cpp
//  Asistente Financiero
//
//  Created by Ramiro on 3/11/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "MenuController.hpp"

MenuController::MenuController()
{
    // Ctor.
}

MenuController::~MenuController()
{
    // Dtor.
}

void MenuController::menu(Datos* data)
{
    bool loopCtrl = false;
    MenuViewer* mv;
    mv = new MenuViewer;
    
    while (!loopCtrl) {
        switch (data->getMenu()) {
            case 0: {
                data->setMenuNoCgi(mv->menu(data));
                break;
            }
            case 1: {
                IngresoController* ic = new IngresoController();
                ic->abm(data);
                delete ic;
                data->setMenuNoCgi(6);
                break;
            }
            case 2: {
                CategoriaIngresoController* cic = new CategoriaIngresoController();
                cic->abm(data);
                delete cic;
                data->setMenuNoCgi(6);
                break;
            }
            case 3: {
                EgresoController* ec = new EgresoController();
                ec->abm(data);
                delete ec;
                data->setMenuNoCgi(6);
                break;
            }
            case 4: {
                AhorroController* ac = new AhorroController();
                ac->abm(data);
                delete ac;
                data->setMenuNoCgi(0);
                data->setMenuNoCgi(mv->menu(data));
                break;
            }
            case 6:
                loopCtrl = true;
                break;
        }
    }
    delete mv;
}


