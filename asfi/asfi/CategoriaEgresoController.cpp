//
//  CategoriaEgresoController.cpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "CategoriaEgresoController.hpp"

CategoriaEgresoController::CategoriaEgresoController()
{
    //ctor
}

CategoriaEgresoController::~CategoriaEgresoController()
{
    //dtor
}

void CategoriaEgresoController::abm(Datos* data)
{
    CategoriaEgresoViewer* cev;
    cev = new CategoriaEgresoViewer();
    
    bool salir = false;
    
    while (!salir)
    {
        switch (data->getAction())
        {
            case 0:
                cev->menu(data);
                data->setActionNoCgi(6);
                break;
            case 1:
                //modificar(idIc);
                if (data->getConfirm() == 1) {
                    modificar(data);
                    data->setActionNoCgi(0);
                }
                cev->menu(data);
                data->setActionNoCgi(6);
                break;
            case 2:
                eliminar(data);
                cev->menu(data);
                data->setActionNoCgi(6);
                break;
            case 3:
                if (data->getConfirm() == 1) {
                    agregar(data);
                    data->setActionNoCgi(0);
                }
                cev->menu(data);
                data->setActionNoCgi(6);
                break;
            case 4:
                buscar(data->getAction(), data);
            case 6:
                salir = true;
                break;
        }
    }
    delete cev;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función listar crea un objeto de tipo EgresoViewer y llama a la fnción listar()
 * de dicho  objeto a la cual se le pasa como argumento la función collection() pertene-
 * ciente al objeto de tipo EgresoDAO. Ver como funciona listar() de la clase EgresoViewer,
 * ver como funciona collection() de la clase EgresoDAO*/
void CategoriaEgresoController::listar(Datos* data)
{
    (new CategoriaEgresoViewer())->listar((new CategoriaEgresoDAO())->collection(), data);
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void CategoriaEgresoController::buscar(int idIc, Datos* datos)
{
    CategoriaEgresoViewer* cev = new CategoriaEgresoViewer();
    cev->mostrar((new CategoriaEgresoDAO())->find(cev->buscar(idIc)), datos);
}

void CategoriaEgresoController::agregar(Datos* datos)
{
    CategoriaEgreso* cl = new CategoriaEgreso();
    cargar(cl, datos);
    (new CategoriaEgresoDAO())->add(cl);
}

int CategoriaEgresoController::eliminar(Datos* datos)
{
    CategoriaEgreso* cl = new CategoriaEgreso();
    CategoriaEgresoViewer* cev = new CategoriaEgresoViewer();
    
    cl = (new CategoriaEgresoDAO())->find(cev->buscar(datos->getId()));
    (new CategoriaEgresoDAO())->del(cl);
    
    return 6;
}

void CategoriaEgresoController::modificar(Datos* datos)
{
    
    CategoriaEgreso* cl = new CategoriaEgreso();
    
    cl = (new CategoriaEgresoDAO())->find(datos->getId());
    this->cargar(cl, datos);
    (new CategoriaEgresoDAO())->update(cl);
}

void CategoriaEgresoController::cargar(CategoriaEgreso *cl, Datos * datos)
{
    cl->setId(datos->getId());
    cl->setNombre(datos->getNombre());
}
