//
//  categoriaIngreso.cpp
//  Asistente Financiero
//
//  Created by rama on 29/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "categoriaIngreso.hpp"

CategoriaIngreso::CategoriaIngreso() {
    this->id = 0;
    this->nombre = "";
}
CategoriaIngreso::CategoriaIngreso(sql::ResultSet* res) {

        this->fillObject(res);
}
void CategoriaIngreso::fillObject(sql::ResultSet* rs)
{
    this->setId(rs->getInt("id"));
    this->setNombre(rs->getString("nombre"));
    
}
CategoriaIngreso::~CategoriaIngreso() {
    
}

void CategoriaIngreso::setId(int a) {
    this->id = a;
}
int CategoriaIngreso::getId() {
    return this->id;
}

void CategoriaIngreso::setNombre(string a) {
    this->nombre = a;
}
string CategoriaIngreso::getNombre() {
    return this->nombre;
}

string CategoriaIngreso::toString(Datos* data)
{
    stringstream id;
    stringstream nombre;
    stringstream urlEdit;
    stringstream urlDel;
    
    id << this->getId();
    nombre << this->getNombre();
    
    if ((data->getMenu() == 1)&&((data->getAction() == 1)||(data->getAction() == 3))) {
        return "<option value='" + id.str() + "'>" + nombre.str() + "</option>\n";
    } else if ((data->getMenu() == 2) &&((data->getAction() == 0)||(data->getAction() == 2))) {
        
        urlEdit << "./asfi.cgi?menu=2&action=1&id=" << this->getId();
        urlDel << "./asfi.cgi?menu=2&action=2&id=" << this->getId();
        
        return "<tr> <td>" + id.str() + "</td> <td>" + nombre.str() + "</td><td> \
         <a href='" + urlEdit.str() + "'><img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></a></td><td> \
         <a href='" + urlDel.str() + "'><img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></a></td></tr>";
    }
    
    
    
    /*return "<tr> <td>" + id.str() + "</td> <td>" + nombre.str() + "</td><td> \
    <a href='" + urlEdit.str() + "'><img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></a></td><td> \
    <a href='" + urlDel.str() + "'><img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></a></td></tr>";*/
    return id.str();
}
