//
//  CategoriaEgresoQueue.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CATEGORIAEGRESOQUEUE_HPP
#define CATEGORIAEGRESOQUEUE_HPP

#include <stdio.h>
#include <iostream>

#include "CategoriaEgresoNode.hpp"
#include "Datos.hpp"

class CategoriaEgresoQueue
{
public:
    CategoriaEgresoNode* qstart;
    CategoriaEgresoNode* qend;
    
    CategoriaEgresoQueue();
    ~CategoriaEgresoQueue();
    void setQStart(CategoriaEgresoNode*);
    CategoriaEgresoNode* getQStart();
    void setQEnd(CategoriaEgresoNode*);
    CategoriaEgresoNode* getQEnd();
    void qstore(CategoriaEgreso*);
    CategoriaEgreso* qretrieve();
    void free();
    void show(Datos* data);
    
protected:
private:
};
#endif /* CategoriaEgresoQueue_hpp */
