//
//  CategoriaEgresoController.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CategoriaEgresoController_hpp
#define CategoriaEgresoController_hpp

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "CategoriaEgresoViewer.hpp"
#include "CategoriaEgresoDAO.hpp"
#include "Datos.hpp"
#include "CategoriaEgreso.hpp"

class CategoriaEgresoController
{
public:
    CategoriaEgresoController();
    virtual ~CategoriaEgresoController();
    void abm(Datos*);
protected:
private:
    void listar(Datos*);
    void buscar(int, Datos*);
    void agregar(Datos*);
    void modificar(Datos*);
    int eliminar(Datos*);
    void cargar(CategoriaEgreso*, Datos*);
};

#endif /* CategoriaEgresoController_hpp */
