//
//  AhorroDAO.hpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef AhorroDAO_hpp
#define AhorroDAO_hpp

#include <stdio.h>
#include <sstream>

#include "Ahorro.hpp"
#include "AhorroQueue.hpp"
#include "MyConnection.hpp"
#include "Datos.hpp"

class AhorroDAO
{
public:
    AhorroQueue* saves();
    
    void del(Datos*);
    
    void update(Datos*);
    
    AhorroQueue* totalAhorro();
    
    AhorroQueue* totalCorriente();
    
    AhorroQueue* disponibleAhorro();
    
    AhorroQueue* disponibleCorriente();
    
};

#endif /* AhorroDAO_hpp */
