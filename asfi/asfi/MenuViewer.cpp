//
//  menuViewer.cpp
//  AsistenteFinanciero
//
//  Created by Ramiro on 3/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "menuViewer.hpp"

MenuViewer::MenuViewer()
{
    
}

MenuViewer::~MenuViewer()
{
    
}

int MenuViewer::menu(Datos* datos)
{
    EgresoViewer* ev = new EgresoViewer();
    AhorroViewer* av = new AhorroViewer();
    
    cout << "Content-Type: text/html\n\r\n\r"
    <<  "<!DOCTYPE html>\n"
    <<  "<html >\n"
    <<  "<head>\n"
    <<  "  <meta charset='UTF-8'>\n"
    <<  "  <title>maquetado html5</title>\n"
    <<  "  <title>Asistente Financiero</title>\n"
    <<  "  <link type='text/css' rel='stylesheet' href='../CSS/style.css'>\n"
    <<  "  <script type='text/javascript' href='../JS/jscript.js'></script>\n"
    <<  "  <link href='https://fonts.googleapis.com/css?family=Sonsie+One' rel='stylesheet'> \n"
    <<  "  <link href='https://fonts.googleapis.com/css?family=Khand' rel='stylesheet'> \n"
    <<  "  <link href='../bootstrap/css/bootstrap.min.css' rel='stylesheet'>\n"
    <<  "  <link href='../bootstrap/css/bootstrap-theme.min.css' rel='stylesheet'>\n"
    <<  "  <link rel='stylesheet' href='css/style.css'>\n"
    <<  "</head>\n"
    <<  "\n"
    <<  "<body>\n"
    <<  "  <div class='content'>\n"
    <<  "   <div class='row'>"
    <<  "    <header>\n"
    <<  "      <a href='./asfi.cgi'><img src='https://cdn2.iconfinder.com/data/icons/money-operations/512/money_box-512.png' alt='logo'></a>\n"
    <<  "      <h1>Big $avings</h1>\n"
    <<  "      <nav>\n"
    <<  "        <ul>\n"
    <<  "          <li><a href='./asfi.cgi?menu=1'>Ingresos</a></li>\n"
    <<  "          <li><a href='./asfi.cgi?menu=3'>Egresos</a></li>\n"
    <<  "          <li><a href='./asfi.cgi?menu=5'>Supermercado</a></li>\n"
    <<  "          <li><a href='./asfi.cgi?menu=7'>Configuración</a></li>\n"
    <<  "        </ul>\n"
    <<  "      </nav>\n"
    <<  "      <article>"
    <<  "      <h2><span style='color:gray;'>Hora actual en</span><br/>Mendoza, Argentina  </h2> \n"
    <<  "      <iframe src='http://www.zeitverschiebung.net/clock-widget-iframe-v2?langua  ge=es&timezone=America%2FArgentina%2FMendoza' width='100%' height='150' frameborder='0'   se  amless></iframe> <small style='color:gray;'></small>\n"
    <<  "      </article>"
    <<  "    </header>\n"
    <<  "    <section>\n";
    ev->listar((new EgresoDAO)->expire(), datos);
    ev->listar((new EgresoDAO)->expired(), datos);
    av->listar((new AhorroDAO)->saves(), datos);
    cout    <<  "    </section>\n"
    <<  "  </div>\n"
    <<  "  </div>\n"
    <<  "</body>\n"
    <<  "</html>\n";
    
    return 6;
}
