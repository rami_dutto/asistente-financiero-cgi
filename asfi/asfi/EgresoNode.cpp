//
//  EgresoNode.cpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "EgresoNode.hpp"

EgresoNode::EgresoNode()
{
    this->egreso = NULL;
    this->next = NULL;
}

EgresoNode::EgresoNode(Egreso* egreso)
{
    this->setEgreso(egreso);
    this->next = NULL;
}

EgresoNode::~EgresoNode()
{
    // Dtor
}

void EgresoNode::setEgreso(Egreso* egreso)
{
    this->egreso = egreso;
    this->next = NULL;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función getElement() devuelve un puntero a un objeto de tipo ingreso*/
Egreso* EgresoNode::getEgreso()
{
    return this->egreso;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void EgresoNode::setNext(EgresoNode* next)
{
    this->next = next;
}

EgresoNode* EgresoNode::getNext()
{
    return this->next;
}

void EgresoNode::show()
{
    cout << egreso->toString();
}
