//
//  EgresoDAO.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef EGRESODAO_HPP
#define EGRESODAO_HPP

#include <stdio.h>
#include <sstream>

#include "Egreso.hpp"
#include "EgresoQueue.hpp"
#include "MyConnection.hpp"


class EgresoDAO
{
public:
    EgresoDAO();
    virtual ~EgresoDAO();
    
    void add(Egreso*);
    
    void update(Egreso*);
    
    bool exist(Egreso*);
    
    void save(Egreso*);
    
    void del(Egreso*);
    
    Egreso* find(int);
    
    EgresoQueue* expire();
    
    EgresoQueue* collection();
    
    EgresoQueue* expired();
    
protected:
private:
};

#endif /* INGRESODAO_hpp */
