//
//  node.cpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "IngresoNode.hpp"

IngresoNode::IngresoNode()
{
    this->ingreso = NULL;
    this->next = NULL;
}

IngresoNode::IngresoNode(Ingreso* ingreso)
{
    this->setIngreso(ingreso);
    this->next = NULL;
}

IngresoNode::~IngresoNode()
{
    // Dtor
}

void IngresoNode::setIngreso(Ingreso* ingreso)
{
    this->ingreso = ingreso;
    this->next = NULL;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función getElement() devuelve un puntero a un objeto de tipo ingreso*/
Ingreso* IngresoNode::getIngreso()
{
    return this->ingreso;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void IngresoNode::setNext(IngresoNode* next)
{
    this->next = next;
}

IngresoNode* IngresoNode::getNext()
{
    return this->next;
}

void IngresoNode::show()
{
    cout << ingreso->toString();
}
