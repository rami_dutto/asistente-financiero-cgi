//
//  AhorroController.cpp
//  asfi
//
//  Created by Ramiro on 9/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "AhorroController.hpp"

AhorroController::AhorroController()
{
    //ctor
}

AhorroController::~AhorroController()
{
    //dtor
}

void AhorroController::abm(Datos* data)
{
    AhorroViewer* av;
    av = new AhorroViewer();
    MenuController* mc;
    mc = new MenuController();
    
    bool salir = false;
    
    while (!salir)
    {
        switch (data->getAction())
        {
            case 0:
                this->listar(data);
                data->setActionNoCgi(6);
            case 1:
                //modificar(idIc);
                modificar(data);
                data->setMenuNoCgi(0);
                mc->menu(data);
                data->setActionNoCgi(6);
                break;
            case 2:
                eliminar(data);
                data->setActionNoCgi(6);
                break;
            /*case 3:
                if (data->getConfirm() == 1) {
                    agregar(data);
                    data->setActionNoCgi(0);
                }
                mc->menu(data);
                data->setActionNoCgi(6);
                break;*/
            case 6:
                salir = true;
                break;
        }
    }
    delete av;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función listar crea un objeto de tipo IngresoViewer y llama a la fnción listar()
 * de dicho  objeto a la cual se le pasa como argumento la función collection() pertene-
 * ciente al objeto de tipo IngresoDAO. Ver como funciona listar() de la clase IngresoViewer,
 * ver como funciona collection() de la clase IngresoDAO*/
void AhorroController::listar(Datos* datos)
{
    (new AhorroViewer())->listar((new AhorroDAO())->saves(), datos);
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*
void AhorroController::buscar(int idIc, Datos* datos)
{
    AhorroViewer* av = new AhorroViewer();
    av->mostrar((new AhorroDAO())->find(av->buscar(idIc)), datos);
}

void AhorroController::agregar(Datos* datos)
{
    Ahorro* ar = new Ahorro();
    cargar(ar, datos);
    (new AhorroDAO())->add(ar);
}
*/
int AhorroController::eliminar(Datos* datos)
{
    AhorroDAO* ad = new AhorroDAO();
    
    ad->del(datos);
    
    delete ad;
    
    return 6;
}


void AhorroController::modificar(Datos* datos)
{
    (new AhorroDAO())->update(datos);
}

void AhorroController::cargar(Ahorro *ar, Datos * datos)
{
    ar->setMonto(datos->getMonto());
    ar->setNombre(datos->getNombre());
    
}
