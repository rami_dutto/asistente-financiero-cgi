//
//  ingreso.hpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef INGRESO_HPP
#define INGRESO_HPP

#include <stdio.h>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>

#include "CategoriaIngresoQueue.hpp"
#include "CategoriaIngresoController.hpp"
#include "Datos.hpp"

using namespace std;

class Ingreso
{
public:
    Ingreso();
    Ingreso(sql::ResultSet*);
    ~Ingreso();
    
    void ingresar();
    
    int getId();
    void setId(int);
    
    void setCategoria(int);
    int getCategoria();
    
    void setDetalle(string);
    string getDetalle();
    
    void setMonto(float);
    float getMonto();
    
    void setAhorro(float);
    float getAhorro();
    
    void setCorriente(float);
    float getCorriente();
    
    void setFecha(string);
    string getFecha();
    
    void setFijo(bool);
    bool getFijo();
    
    void fillObject(sql::ResultSet*);
    string toString();
    
protected:
    
private:
    int id;
    int categoria;
    float monto;
    float ahorro;
    float corriente;
    string fecha;
    string detalle;
    bool fijo;
};

#endif /* INGRESO_HPP */
