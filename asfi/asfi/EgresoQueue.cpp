//
//  EgresoQueue.cpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "EgresoQueue.hpp"

EgresoQueue::EgresoQueue()
{
    qstart = qend = NULL;
}

EgresoQueue::~EgresoQueue()
{
    
}

void EgresoQueue::setQStart(EgresoNode* qstart)
{
    this->qstart = qstart;
}

EgresoNode* EgresoQueue::getQStart()
{
    return this->qstart;
}

void EgresoQueue::setQEnd(EgresoNode* qend)
{
    this->qend = qend;
}

EgresoNode* EgresoQueue::getQEnd()
{
    return this->qend;
}

void EgresoQueue::qstore(Egreso* element)
{
    EgresoNode* newNode = new EgresoNode();
    
    newNode->setEgreso(element);
    newNode->setNext(NULL);
    
    if (qstart == NULL)
    {
        qstart = qend = newNode;
        return;
    }
    
    qend->setNext(newNode); //Hace que el último apunte al nuevo último
    
    qend = newNode; // Hace que el último sea el nuevo
}

Egreso* EgresoQueue::qretrieve()
{
    if (this->qstart == NULL)
        return NULL;
    Egreso* aux = this->qstart->getEgreso(); //Recupero elemento a devolver
    EgresoNode* nx = this->qstart; // Apunto nodo para no perder la referencia
    
    this->qstart = nx->getNext(); // Corro el start al siguiente nodo
    
    if (this->qstart == NULL)
        this->qend = NULL;
    
    delete nx; // Elimino el que era el primer nodo de la cola
    
    return aux; // Devuelvo el elemento recuperado
    
}

void EgresoQueue::free()
{
    while (this->qretrieve() != NULL);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */
void EgresoQueue::show()
{
    /* qstar es un puntero a un objeto de tipo node y apunta al primer nodo de la cola
     * si qstar equivale a NULL, no apunta a ningun nodo y show() termina, si no, con-
     * tinua
     if (this->qstart == NULL)
     return;
     */
    /* si se sala el 'if' se crea aux que es un puntero a un objeto de tipo node y se
     * apunta al mismo nodo que qstar, es decir; el primer nodo de la lista*/
    EgresoNode* aux = this->qstart;
    /* Mientras aux sea distinti de NULL se ejecuta el while */
    while (aux)
    {
        aux->show();
        /* Al ser aux un puntero a node, este puede invocar los metodos de node, asi que
         * invoca el metodo getElement() de node */
        cout << endl;
        /* Hago que aux apunte a la dirección de memoria del siguiente objeto node*/
        aux = aux->getNext();
    }
    
    cout << endl;
    
}
