//
//  categoriaEgreso.cpp
//  Asistente Financiero
//
//  Created by rama on 29/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "categoriaEgreso.hpp"

CategoriaEgreso::CategoriaEgreso() {
    this->id = 0;
    this->nombre = "";
}
CategoriaEgreso::CategoriaEgreso(sql::ResultSet* res) {
    
    this->fillObject(res);
}
void CategoriaEgreso::fillObject(sql::ResultSet* rs)
{
    this->setId(rs->getInt("id"));
    this->setNombre(rs->getString("nombre"));
    
}
CategoriaEgreso::~CategoriaEgreso() {
    
}

void CategoriaEgreso::setId(int a) {
    this->id = a;
}
int CategoriaEgreso::getId() {
    return this->id;
}

void CategoriaEgreso::setNombre(string a) {
    this->nombre = a;
}
string CategoriaEgreso::getNombre() {
    return this->nombre;
}

string CategoriaEgreso::toString(Datos* data)
{
    stringstream id;
    stringstream nombre;
    stringstream urlEdit;
    stringstream urlDel;
    
    id << this->getId();
    nombre << this->getNombre();
    
    if ((data->getMenu() == 3)&&((data->getAction() == 1)||(data->getAction() == 3))) {
        return "<option value='" + id.str() + "'>" + nombre.str() + "</option>\n";
    } else if ((data->getMenu() == 4) &&((data->getAction() == 0)||(data->getAction() == 2))) {
        
        urlEdit << "./asfi.cgi?menu=4&action=1&id=" << this->getId();
        urlDel << "./asfi.cgi?menu=4&action=2&id=" << this->getId();
        
        return "<tr> <td>" + id.str() + "</td> <td>" + nombre.str() + "</td><td> \
        <a href='" + urlEdit.str() + "'><img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></a></td><td> \
        <a href='" + urlDel.str() + "'><img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></a></td></tr>";
    }
    
    
    
    /*return "<tr> <td>" + id.str() + "</td> <td>" + nombre.str() + "</td><td> \
     <a href='" + urlEdit.str() + "'><img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></a></td><td> \
     <a href='" + urlDel.str() + "'><img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></a></td></tr>";*/
    return id.str();
}
