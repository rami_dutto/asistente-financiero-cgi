//
//  AhorroController.hpp
//  asfi
//
//  Created by Ramiro on 9/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef AhorroController_hpp
#define AhorroController_hpp

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "AhorroViewer.hpp"
#include "AhorroDAO.hpp"
#include "Datos.hpp"
#include "Ahorro.hpp"
#include "MenuController.hpp"

class AhorroController
{
public:
    AhorroController();
    virtual ~AhorroController();
    void abm(Datos*);
protected:
private:
    void listar(Datos*);
    /*void buscar(int, Datos*);
    void agregar(Datos*);*/
    void modificar(Datos*);
    int eliminar(Datos*);
    void cargar(Ahorro*, Datos*);
};
#endif /* AhorroController_hpp */
