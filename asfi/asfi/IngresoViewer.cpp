//
//  ingresoViewer.cpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//
#include "ingresoViewer.hpp"

/* CREACION DEL CONSTRUCTOR */
IngresoViewer::IngresoViewer()
{
    //ctor
}

/* CREACION DEL DESTRUCTOR */
IngresoViewer::~IngresoViewer()
{
    //dtor
}

int IngresoViewer::menu(Datos* datos)
{
    IngresoViewer* iv = new IngresoViewer();
    
    cout << "Content-Type: text/html\n\r\n\r"
    <<  "<!DOCTYPE html>\n"
    <<  "<html >\n"
    <<  "<head>\n"
    <<  "  <meta charset='UTF-8'>\n"
    <<  "  <title>maquetado html5</title>\n"
    <<  "  <title>Asistente Financiero</title>\n"
    <<  "  <link type='text/css' rel='stylesheet' href='../CSS/style.css'>\n"
    <<  "  <script type='text/javascript' href='../JS/jscript.js'></script>\n"
    <<  "  <link href='https://fonts.googleapis.com/css?family=Sonsie+One' rel='stylesheet'> \n"
    <<  "  <link href='https://fonts.googleapis.com/css?family=Khand' rel='stylesheet'> \n"
    <<  "  <link href='../bootstrap/css/bootstrap.min.css' rel='stylesheet'>\n"
    <<  "  <link href='../bootstrap/css/bootstrap-theme.min.css' rel='stylesheet'>\n"
    <<  "  <link rel='stylesheet' href='css/style.css'>\n"
    <<  "</head>\n"
    <<  "\n"
    <<  "<body>\n"
    <<  "  <div class='content'>\n"
    <<  "   <div class='row'>"
    <<  "    <header>\n"
    <<  "      <a href='./asfi.cgi'><img src='https://cdn2.iconfinder.com/data/icons/money-operations/512/money_box-512.png' alt='logo'></a>\n"
    <<  "      <h1>Big $avings</h1>\n"
    <<  "      <nav>\n"
    <<  "        <ul>\n"
    <<  "          <li><a href='./asfi.cgi?menu=1'>Ingresos</a></li>\n"
    <<  "          <li><a href='./asfi.cgi?menu=3'>Egresos</a></li>\n"
    <<  "          <li><a href='./asfi.cgi?menu=5'>Supermercado</a></li>\n"
    <<  "          <li><a href='./asfi.cgi?menu=7'>Configuración</a></li>\n"
    <<  "        </ul>\n"
    <<  "      </nav>\n"
    <<  "      <article>"
    <<  "      <h2><span style='color:gray;'>Hora actual en</span><br/>Mendoza, Argentina  </h2> \n"
    <<  "      <iframe src='http://www.zeitverschiebung.net/clock-widget-iframe-v2?langua  ge=es&timezone=America%2FArgentina%2FMendoza' width='100%' height='150' frameborder='0'   se  amless></iframe> <small style='color:gray;'></small>\n"
    <<  "      </article>"
    <<  "    </header>\n"
    <<  "    <section>\n";
    
    if ((datos->getAction() == 0) || (datos->getAction() == 2)) {
        iv->listar((new IngresoDAO())->collection());
        
    } else if ((datos->getAction() == 1) || (datos->getAction() == 3)) {
        iv->mostrar((new IngresoDAO())->find(iv->buscar(datos->getId())), datos);
    }
    cout <<  "    </section>\n"
    /* <<  "    <aside>\n"
    <<  "    </aside>\n"*/
    <<  "  </div>\n"
    <<  "  </div>\n"
    <<  "</body>\n"
    <<  "</html>\n";
    
    return 6;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función listar tiene como parametro un puntero a un objeto queue(Cola) al 
 * al invocar a la funcion listar se le pasa como argumento un puntero a un bojeto queue
 * y se invoca la función show() de dicho objeto. En el archivo queue.cpp se detalla el 
 * funcionamiento de la función show()*/
void IngresoViewer::listar(IngresoQueue* ingresoQueue)
{
    AhorroViewer* av = new AhorroViewer();
    
    cout <<  "<div id='constrainer'>\n"
    <<  "<div class='scrolltable'>\n"
    <<  "<table class='header'><thead><th>Ahorro</th><th>A gastos corrientes</th><th>Catergoría</th><th>Monto</th><th>Descripción</th><th>Fecha Entrada</th><th>Modificar</th><th>Eliminar</th></thead></table>\n"
    <<  "<div class='body'>\n"
    <<  "<table>\n"
    <<  "<tbody>\n";
    ingresoQueue->show();
    cout << "</tbody>\n"
    <<  "</table>\n"
    <<  "</div>\n"
    <<  "<table>\n"
    <<  "<a class='footer' href='./asfi.cgi?menu=1&action=3'>\n"
    <<  "<tf>Agregar Ingreso</tf>\n"
    <<  "</a>\n"
    <<  "</table>\n"
    <<  "</div>\n"
    <<  "</div>\n"
    <<  "<div style='float: left; margin-top: 28px;'>";
    av->listarTipo((new AhorroDAO)->totalAhorro(), 0);
    av->listarTipo((new AhorroDAO)->totalCorriente(), 1);
    av->listaCantidades((new AhorroDAO)->disponibleAhorro(), 0);
    av->listaCantidades((new AhorroDAO)->disponibleCorriente(), 1);
    cout << "</div>";
    delete av;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/




int IngresoViewer::buscar(int idIv)
{
    int id = idIv;
    
    return id;
}
/* Funcin para mostrar todos los valores de un Ingreso */
void IngresoViewer::mostrar(Ingreso* in, Datos* datos)
{
    CategoriaIngresoViewer* civ = new CategoriaIngresoViewer();
    string menu;
    string action;
    stringstream id;
    stringstream monto;
    stringstream detalle;
    stringstream fecha;
    stringstream ahorro;
    stringstream fijo;
    
    if (datos->getAction() == 1) {
        id << "value = '" << in->getId() << "'";
        monto << "value = '" << in->getMonto() << "'";
        detalle << "value = '" << in->getDetalle() <<"'";
        fecha << "value = '" << in->getFecha() << "'";
        action = "1";
        ahorro << "value = '" << in->getAhorro() <<"' min = '0' max = '" << in->getMonto() <<"'";
    } else {
        id << "";
        monto << "";
        detalle << "";
        fecha << "";
        action = "3";
        ahorro << "";
    }
    
    
    /* cout <<  "<article class='add'>\n"*/
    cout <<  "<form name='form1' action='./asfi.cgi' method='get'>\n"
    <<  "<div class='categoria_add'>"
    <<  "<p>CATEGORIA</p>"
    <<  "Editar:<a href='./asfi.cgi?menu=2&action=0'>Add-Edit categoría</a>\n"
    <<  "<br/>\n"
    <<  "Seleccionar: <select name='cat_id'>\n";
    
    civ->listar((new CategoriaIngresoDAO())->collection(), datos);
    // <<  "<option>1</option>\n"
    
    cout <<  "</select>\n"
    <<  "</div>\n"
    <<  "<p>"
    <<  "Tipo de Ingreso:<select name='fijo'>"
    <<  "<option value='1'>Fijo Mensual</option>"
    <<  "<option value='0'>Temporal</option>"
    <<  "</select><br/>\n"
    <<  "<input type='text' name='menu' value='1' style='display: none'>\n"
    <<  "<input type='text' name='action' value='" + action + "' style='display: none'>\n"
    <<  "<input type='text' name='confirm' value='1' style='display: none'>\n"
    <<  "Monto: <input type='text' name='monto' min='0' max='999999' " + monto.str() + " required />\n"
    <<  "<br />"
    <<  "Cantidad para ahorro:<input type='text' name='ahorro' " + ahorro.str() + " required>\n"
    <<  "<input type='text' name='id' " + id.str() + " style='display: none'>\n"
    <<  "Descripción: <input type='text' name='des' " + detalle.str() + " required />\n"
    <<  "<br />\n"
    <<  "Fecha de entrada: <input type='date' " + fecha.str() + " name='fecha' />\n"
    <<  "<br />\n"
    <<  "<input type='submit' value='Confirmar' /><input type='button' value='Cancelar' onClick=\"window.location='./asfi.cgi?menu=1'\">\n"
    <<  "</p>\n"
    <<  "</form>\n";
    /* <<  "       </article>\n";*/
}



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

