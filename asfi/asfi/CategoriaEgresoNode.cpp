//
//  CategoriaEgresoNode.cpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "CategoriaEgresoNode.hpp"

CategoriaEgresoNode::CategoriaEgresoNode()
{
    this->categoriaEgreso = NULL;
    this->next = NULL;
}

CategoriaEgresoNode::CategoriaEgresoNode(CategoriaEgreso* categoriaEgreso)
{
    this->setCategoriaEgreso(categoriaEgreso);
    this->next = NULL;
}

CategoriaEgresoNode::~CategoriaEgresoNode()
{
    // Dtor
}

void CategoriaEgresoNode::setCategoriaEgreso(CategoriaEgreso* categoriaEgreso)
{
    this->categoriaEgreso = categoriaEgreso;
    this->next = NULL;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función getElement() devuelve un puntero a un objeto de tipo Egreso*/
CategoriaEgreso* CategoriaEgresoNode::getCategoriaEgreso()
{
    return this->categoriaEgreso;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void CategoriaEgresoNode::setNext(CategoriaEgresoNode* next)
{
    this->next = next;
}

CategoriaEgresoNode* CategoriaEgresoNode::getNext()
{
    return this->next;
}

void CategoriaEgresoNode::show(Datos* data)
{
    cout << categoriaEgreso->toString(data);
}
