//
//  AhorroQueue.cpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "AhorroQueue.hpp"

AhorroQueue::AhorroQueue()
{
    qstart = qend = NULL;
}

AhorroQueue::~AhorroQueue()
{
    
}

void AhorroQueue::setQStart(AhorroNode* qstart)
{
    this->qstart = qstart;
}

AhorroNode* AhorroQueue::getQStart()
{
    return this->qstart;
}

void AhorroQueue::setQEnd(AhorroNode* qend)
{
    this->qend = qend;
}

AhorroNode* AhorroQueue::getQEnd()
{
    return this->qend;
}

void AhorroQueue::qstore(Ahorro* element)
{
    AhorroNode* newNode = new AhorroNode();
    
    newNode->setAhorro(element);
    newNode->setNext(NULL);
    
    if (qstart == NULL)
    {
        qstart = qend = newNode;
        return;
    }
    
    qend->setNext(newNode); //Hace que el último apunte al nuevo último
    
    qend = newNode; // Hace que el último sea el nuevo
}

Ahorro* AhorroQueue::qretrieve()
{
    if (this->qstart == NULL)
        return NULL;
    Ahorro* aux = this->qstart->getAhorro(); //Recupero elemento a devolver
    AhorroNode* nx = this->qstart; // Apunto nodo para no perder la referencia
    
    this->qstart = nx->getNext(); // Corro el start al siguiente nodo
    
    if (this->qstart == NULL)
        this->qend = NULL;
    
    delete nx; // Elimino el que era el primer nodo de la cola
    
    return aux; // Devuelvo el elemento recuperado
    
}

void AhorroQueue::free()
{
    while (this->qretrieve() != NULL);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */
void AhorroQueue::show(int a)
{
    /* qstar es un puntero a un objeto de tipo node y apunta al primer nodo de la cola
     * si qstar equivale a NULL, no apunta a ningun nodo y show() termina, si no, con-
     * tinua
     if (this->qstart == NULL)
     return;
     */
    /* si se sala el 'if' se crea aux que es un puntero a un objeto de tipo node y se
     * apunta al mismo nodo que qstar, es decir; el primer nodo de la lista*/
    AhorroNode* aux = this->qstart;
    /* Mientras aux sea distinti de NULL se ejecuta el while */
    while (aux)
    {
        aux->show(a);
        /* Al ser aux un puntero a node, este puede invocar los metodos de node, asi que
         * invoca el metodo getElement() de node */
        cout << endl;
        /* Hago que aux apunte a la dirección de memoria del siguiente objeto node*/
        aux = aux->getNext();
    }
    
    cout << endl;
    
}
