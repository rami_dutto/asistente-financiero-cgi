//
//  dbconnection.hpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef MYCONNECTION_HPP
#define MYCONNECTION_HPP

#include <stdio.h>
#include <mysql_connection.h>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cstddef>

using namespace std;

class MyConnection {
public:
    virtual ~MyConnection();
    MyConnection();
    static MyConnection* instance();
    void connect();
    void disconnect();
    sql::Connection* getConnection();
    sql::ResultSet* query(string);
    void execute(string);
protected:
    
    
private:
    static MyConnection* pInstance;
    sql::Connection* connection;
};

#endif /* dbconnection_hpp */
