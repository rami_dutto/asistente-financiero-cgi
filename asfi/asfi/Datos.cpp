//
//  Datos.cpp
//  ingresos
//
//  Created by Ramiro on 20/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "Datos.hpp"

Datos::Datos()
{
    this->setMenuNoCgi(0);
    this->setActionNoCgi(0);
    this->setConfirmNoCgi(0);
    this->setFechaNoCgi("");
    this->setDescripcionCgi("");
    this->setNombreNoCgi("");
    this->setMontoNoCgi(0);
    this->setAhorroNoCgi(0);
    this->setCorrienteNoCgi(0);
    this->setFijoNoCgi(0);
    this->setPagadoNoCgi(0);
    
}

Datos::~Datos()
{
    // Dtor.
}

void Datos::setMenu(string a)
{
    cgicc::form_iterator fi = formData.getElement(a);
    
    if( !fi->isEmpty() && fi != (*formData).end()) {
        this->menu = stoi(**fi);
    }else{
        this->menu = 0;
    }
}
void Datos::setMenuNoCgi(int a)
{
    this->menu = a;
}
int Datos::getMenu()
{
    return this->menu;
}

void Datos::setAction(string a)
{
    form_iterator fi = formData.getElement(a);
    
    if( !fi->isEmpty() && fi != (*formData).end()) {
        this->action = stoi(**fi);
    }else{
        this->action = 0;
    }
}
void Datos::setActionNoCgi(int a)
{
    this->action = a;
}
int Datos::getAction()
{
    return this->action;
}

void Datos::setConfirm(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        this->confirm = 1;
    }else{
        this->confirm = 0;
    }
}
void Datos::setConfirmNoCgi(int a)
{
    this->confirm = a;
}
int Datos::getConfirm()
{
    return this->confirm;
}

void Datos::setId(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        this->id = stoi(**fi);
    }else{
        this->id = 0;
    }
}
void Datos::setIdNoCgi(int a)
{
    this->id = a;
}
int Datos::getId()
{
    return this->id;
}

void Datos::setCatId(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        this->cat_id = stoi(**fi);
    }else{
        this->cat_id = 0;
    }
}
void Datos::setCatIdNoCgi(int a)
{
    this->cat_id = a;
}
int Datos::getCatID()
{
    return this->cat_id;
}

void Datos::setDescripcion(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        this->descripcion = **fi;
    }else{
        this->descripcion = "";
    }
}
void Datos::setDescripcionCgi(string a)
{
    this->descripcion = a;
}
string Datos::getDescripcion()
{
    return this->descripcion;
}

void Datos::setFecha(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        this->fecha = **fi;
    }else{
        this->fecha = "";
    }
}
void Datos::setFechaNoCgi(string a)
{
    this->fecha = a;
}
string Datos::getFecha()
{
    return this->fecha;
}

void Datos::setMonto(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        this->monto = stof(**fi);
    }else{
        this->monto = 0;
    }
}
void Datos::setMontoNoCgi(float a)
{
    this->monto = a;
}
float Datos::getMonto()
{
    return this->monto;
}

void Datos::setAhorro(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        this->ahorro = stof(**fi);
    }else{
        this->ahorro = 0;
    }
}
void Datos::setAhorroNoCgi(float a)
{
    this->ahorro = a;
}
float Datos::getAhorro()
{
    return this->ahorro;
}

void Datos::setCorriente(string monto, string ahorro )
{
    form_iterator fi = formData.getElement(monto);
    form_iterator fi2 = formData.getElement(ahorro);
    if( (!fi->isEmpty() &&fi != (*formData).end()) && (!fi2->isEmpty() &&fi2 != (*formData).end())) {
        this->corriente = (stof(**fi) - stof(**fi2));
    }else{
        this->corriente = 0;
    }
}
void Datos::setCorrienteNoCgi(float a)
{
    this->corriente = a;
}
float Datos::getCorriente()
{
    return this->corriente;
}

void Datos::setFijo(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        if ( stoi(**fi) == 1) {
            this->fijo = 1;
        }else{
            this->fijo = 0;
        }
    }else{
        this->fijo = 0;
    }
}
void Datos::setFijoNoCgi(int a)
{
    this->fijo = a;
}
int Datos::getFijo()
{
    return this->fijo;
}

void Datos::setNombre(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        this->nombre = **fi;
    }else{
        this->nombre = "";
    }
}
void Datos::setNombreNoCgi(string a)
{
    this->nombre = a;
}
string Datos::getNombre()
{
    return this->nombre;
}

void Datos::setPagado(string a)
{
    form_iterator fi = formData.getElement(a);
    if( !fi->isEmpty() &&fi != (*formData).end()) {
        if ( stoi(**fi) == 1) {
            this->pagado = 1;
        }else{
            this->pagado = 0;
        }
    }else{
        this->pagado = 0;
    }
}
void Datos::setPagadoNoCgi(int a)
{
    this->pagado = a;
}
int Datos::getPagado()
{
    return this->pagado;
}


