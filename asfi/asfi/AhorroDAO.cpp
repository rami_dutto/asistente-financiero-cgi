//
//  AhorroDAO.cpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "AhorroDAO.hpp"

AhorroQueue* AhorroDAO::saves()
{
    AhorroQueue* ahorroQueue = new AhorroQueue();
    stringstream stringSQL;
    
    stringSQL << "SELECT detalle, SUM(monto) as suma FROM egreso\
                 WHERE id_tipo_de_egreso = 1 AND pagado = 0 GROUP BY detalle ORDER BY suma;";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    while (res->next())
        ahorroQueue->qstore(new Ahorro(res));
    
    delete res;
    
    cout << "<h1 class='ingresos'>Ahorros</h1>";
    
    return ahorroQueue;
}

void AhorroDAO::del(Datos* datos)
{
    stringstream stringSQL;
    
    stringSQL << "DELETE FROM egreso WHERE detalle = "
    << "'" << datos->getNombre() << "';";
    
    MyConnection::instance()->execute(stringSQL.str());
}

void AhorroDAO::update(Datos* datos)
{
    stringstream stringSQL;
    
    stringSQL << "UPDATE egreso SET pagado='1' WHERE detalle='"
              << datos->getNombre() << "';";
    
    MyConnection::instance()->execute(stringSQL.str());
}

AhorroQueue* AhorroDAO::totalAhorro()
{
    AhorroQueue* ahorroQueue = new AhorroQueue();
    stringstream stringSQL;
    
    stringSQL << "SELECT SUM(ahorro) AS suma, COUNT(*) AS detalle FROM ingreso;";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    while (res->next())
        ahorroQueue->qstore(new Ahorro(res));
    
    delete res;
    
    return ahorroQueue;
}

AhorroQueue* AhorroDAO::totalCorriente()
{
    AhorroQueue* ahorroQueue = new AhorroQueue();
    stringstream stringSQL;
    
    stringSQL << "SELECT SUM(corriente) AS suma, COUNT(*) AS detalle FROM ingreso;";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    while (res->next())
        ahorroQueue->qstore(new Ahorro(res));
    
    delete res;
    
    return ahorroQueue;
}

AhorroQueue* AhorroDAO::disponibleAhorro()
{
    AhorroQueue* ahorroQueue = new AhorroQueue();
    stringstream stringSQL;
    
    stringSQL << "SELECT SUM(valor1-valor2) AS suma, COUNT(*) AS detalle FROM (SELECT IFNULL(SUM(ahorro),0)\
                 as valor1 FROM ingreso) as ahorro INNER JOIN (SELECT IFNULL(SUM(monto),0) as valor2 FROM\
                 egreso WHERE id_tipo_de_egreso = 1) AS monto;";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    while (res->next())
        ahorroQueue->qstore(new Ahorro(res));
    
    delete res;
    
    return ahorroQueue;
}

AhorroQueue* AhorroDAO::disponibleCorriente()
{
    AhorroQueue* ahorroQueue = new AhorroQueue();
    stringstream stringSQL;
    
    stringSQL << "SELECT SUM(valor1-valor2) AS suma, COUNT(*) AS detalle FROM (SELECT IFNULL(SUM(corriente),0)\
              as valor1 FROM ingreso) as ahorro INNER JOIN (SELECT IFNULL(SUM(monto),0) as valor2 FROM\
              egreso WHERE id_tipo_de_egreso != 1 AND pagado = 0) AS monto;";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    while (res->next())
        ahorroQueue->qstore(new Ahorro(res));
    
    delete res;
    
    return ahorroQueue;
}

