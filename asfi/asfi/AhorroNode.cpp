//
//  AhorroNode.cpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "AhorroNode.hpp"

AhorroNode::AhorroNode()
{
    this->ahorro = NULL;
    this->next = NULL;
}

AhorroNode::AhorroNode(Ahorro* ahorro)
{
    this->setAhorro(ahorro);
    this->next = NULL;
}

AhorroNode::~AhorroNode()
{
    // Dtor
}

void AhorroNode::setAhorro(Ahorro* ahorro)
{
    this->ahorro = ahorro;
    this->next = NULL;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función getElement() devuelve un puntero a un objeto de tipo ingreso*/
Ahorro* AhorroNode::getAhorro()
{
    return this->ahorro;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void AhorroNode::setNext(AhorroNode* next)
{
    this->next = next;
}

AhorroNode* AhorroNode::getNext()
{
    return this->next;
}

void AhorroNode::show(int a)
{
    cout << ahorro->toString(a);
}
