//
//  egreso.hpp
//  Asistente Financiero
//
//  Created by rama on 28/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef Egreso_HPP
#define Egreso_HPP

#include <stdio.h>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>

#include "CategoriaEgresoQueue.hpp"
#include "CategoriaEgresoController.hpp"
#include "Datos.hpp"

using namespace std;

class Egreso
{
public:
    Egreso();
    Egreso(sql::ResultSet*);
    ~Egreso();
    
    void ingresar();
    
    int getId();
    void setId(int);
    
    void setCategoria(int);
    int getCategoria();
    
    void setDetalle(string);
    string getDetalle();
    
    void setMonto(float);
    int getMonto();
    
    void setFecha(string);
    string getFecha();
    
    void setPagado(int);
    int getPagado();
    
    void fillObject(sql::ResultSet*);
    string toString();
    
    
protected:
    
private:
    int id;
    float monto;
    string fecha;
    int categoria;
    string detalle;
    int pagado;
};

#endif /* egreso_hpp */
