//
//  CategoriaEgresoNode.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CategoriaEgresoNode_hpp
#define CategoriaEgresoNode_hpp

#include <stdio.h>

#include "CategoriaEgreso.hpp"
#include "Datos.hpp"

class CategoriaEgresoNode
{
public:
    CategoriaEgresoNode();
    CategoriaEgresoNode(CategoriaEgreso*);
    virtual ~CategoriaEgresoNode();
    void setCategoriaEgreso(CategoriaEgreso*);
    CategoriaEgreso* getCategoriaEgreso();
    void setNext(CategoriaEgresoNode*);
    CategoriaEgresoNode* getNext();
    void show(Datos*);
protected:
    
private:
    CategoriaEgreso* categoriaEgreso;
    CategoriaEgresoNode* next;
};

#endif /* CategoriaEgresoNode_hpp */
