//
//  Datos.hpp
//  ingresos
//
//  Created by Ramiro on 20/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef DATOS_HPP
#define DATOS_HPP

#include <stdio.h>
#include <string>

#include <cgicc/CgiDefs.h>
#include <cgicc/Cgicc.h>
#include <cgicc/HTTPHTMLHeader.h>
#include <cgicc/HTMLClasses.h>

using namespace std;
using namespace cgicc;

class Datos {
public:
    
    Datos();
    ~Datos();
    
    void setMenu(string);
    void setMenuNoCgi(int);
    int getMenu();
    
    void setAction(string);
    void setActionNoCgi(int);
    int getAction();
    
    void setConfirm(string);
    void setConfirmNoCgi(int);
    int getConfirm();
    
    void setId(string);
    void setIdNoCgi(int);
    int getId();
    
    void setCatId(string);
    void setCatIdNoCgi(int);
    int getCatID();
    
    void setDescripcion(string);
    void setDescripcionCgi(string);
    string getDescripcion();
    
    void setFecha(string);
    void setFechaNoCgi(string);
    string getFecha();
    
    void setNombre(string);
    void setNombreNoCgi(string);
    string getNombre();
    
    void setMonto(string);
    void setMontoNoCgi(float);
    float getMonto();
    
    void setAhorro(string);
    void setAhorroNoCgi(float);
    float getAhorro();
    
    void setCorriente(string, string);
    void setCorrienteNoCgi(float);
    float getCorriente();
    
    void setFijo(string);
    void setFijoNoCgi(int);
    int getFijo();
    
    void setPagado(string);
    void setPagadoNoCgi(int);
    int getPagado();
    
private:
    int menu; //Controla las deciciones de menu controller
    int action; // Controla las acciones a tomar en cada tabla, agregar, modificar, buscar, eliminar, etc.
    int confirm; // Confirmar acción.
    int id; // id del registro.
    int cat_id; // cat_id del registro.
    string descripcion; // descripción del registro.
    string fecha;   // fecha del registro.
    string nombre;
    float monto;    // monto del registro.
    float ahorro;   // Se setea con ingresos, cantidad destinada a ahorros.
    float corriente; // Se setea con ingresos, cantidad destinada a corrientes.
    int fijo; // Booleando que determina si es un ingreso mensual o temporal.
    int pagado; // Para servicios e impuestos en egresos.
    Cgicc formData;
    
};

#endif /* Datos_hpp */
