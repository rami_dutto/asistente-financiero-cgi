//
//  menuViewer.hpp
//  Asistente Financiero
//
//  Created by Ramiro on 3/11/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef MENUVIEWER_HPP
#define MENUVIEWER_HPP

#include <stdio.h>
#include <iostream>

using namespace std;

#include "EgresoViewer.hpp"
#include "IngresoViewer.hpp"
#include "AhorroViewer.hpp"

class MenuViewer
{
public:
    MenuViewer();
    virtual ~MenuViewer();
    int menu(Datos*);
protected:
private:
    //int optionMC;
};

#endif /* menuViewer_hpp */
