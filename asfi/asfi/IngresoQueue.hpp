//
//  queue.hpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef INGRESOQUEUE_HPP
#define INGRESOQUEUE_HPP

#include <stdio.h>
#include <iostream>

#include "IngresoNode.hpp"

using namespace std;

class IngresoQueue
{
public:
    IngresoQueue();
    ~IngresoQueue();
    void setQStart(IngresoNode*);
    IngresoNode* getQStart();
    void setQEnd(IngresoNode*);
    IngresoNode* getQEnd();
    void qstore(Ingreso*);
    Ingreso* qretrieve();
    void free();
    void show();
    
protected:
    
private:
    IngresoNode* qstart;
    IngresoNode* qend;
};

#endif /* queue_hpp */
