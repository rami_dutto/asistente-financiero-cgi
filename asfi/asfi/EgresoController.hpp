//
//  EgresoController.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef EGRESOCONTROLLER_HPP
#define EGRESOCONTROLLER_HPP

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "EgresoViewer.hpp"
#include "CategoriaEgresoViewer.hpp"
#include "EgresoDAO.hpp"
#include "CategoriaEgresoDAO.hpp"
#include "Datos.hpp"
#include "Egreso.hpp"

class EgresoController
{
public:
    EgresoController();
    virtual ~EgresoController();
    void abm(Datos*);
protected:
private:
    void listar(Datos*);
    void buscar(int, Datos*);
    void agregar(Datos*);
    void modificar(Datos*);
    int eliminar(Datos*);
    void cargar(Egreso*, Datos*);
};

#endif /* EgresoController_hpp */
