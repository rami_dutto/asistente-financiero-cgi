//
//  EgresoController.cpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "EgresoController.hpp"

EgresoController::EgresoController()
{
    //ctor
}

EgresoController::~EgresoController()
{
    //dtor
}

void EgresoController::abm(Datos* data)
{
    EgresoViewer* ev;
    ev = new EgresoViewer();
    
    bool salir = false;
    
    while (!salir)
    {
        switch (data->getAction())
        {
            case 0:
                ev->menu(data);
                data->setActionNoCgi(6);
                break;
            case 1:
                //modificar(idIc);
                if (data->getConfirm() == 1) {
                    modificar(data);
                    data->setActionNoCgi(0);
                }
                ev->menu(data);
                data->setActionNoCgi(6);
                break;
            case 2:
                eliminar(data);
                ev->menu(data);
                data->setActionNoCgi(6);
                break;
            case 3:
                if (data->getConfirm() == 1) {
                    agregar(data);
                    data->setActionNoCgi(0);
                }
                ev->menu(data);
                data->setActionNoCgi(6);
                break;
            case 6:
                salir = true;
                break;
        }
    }
    delete ev;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función listar crea un objeto de tipo IngresoViewer y llama a la fnción listar()
 * de dicho  objeto a la cual se le pasa como argumento la función collection() pertene-
 * ciente al objeto de tipo IngresoDAO. Ver como funciona listar() de la clase IngresoViewer,
 * ver como funciona collection() de la clase IngresoDAO*/
void EgresoController::listar(Datos* datos)
{
    (new EgresoViewer())->listar((new EgresoDAO())->collection(), datos);
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void EgresoController::buscar(int idIc, Datos* datos)
{
    EgresoViewer* ev = new EgresoViewer();
    ev->mostrar((new EgresoDAO())->find(ev->buscar(idIc)), datos);
}

void EgresoController::agregar(Datos* datos)
{
    Egreso* cl = new Egreso();
    cargar(cl, datos);
    (new EgresoDAO())->add(cl);
}

int EgresoController::eliminar(Datos* datos)
{
    Egreso* cl = new Egreso();
    EgresoViewer* cv = new EgresoViewer();
    
    cl = (new EgresoDAO())->find(cv->buscar(datos->getId()));
    (new EgresoDAO())->del(cl);
    
    return 6;
}

void EgresoController::modificar(Datos* datos)
{
    
    Egreso* cl = new Egreso();
    
    cl = (new EgresoDAO())->find(datos->getId());
    this->cargar(cl, datos);
    (new EgresoDAO())->update(cl);
}

void EgresoController::cargar(Egreso *cl, Datos * datos)
{
    cl->setId(datos->getId());
    cl->setDetalle(datos->getDescripcion());
    cl->setMonto(datos->getMonto());
    cl->setFecha(datos->getFecha());
    cl->setCategoria(datos->getCatID());
    cl->setPagado(datos->getPagado());
    
}
