//
//  AhorroViewer.hpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef AhorroViewer_hpp
#define AhorroViewer_hpp

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "AhorroDAO.hpp"
#include "AhorroQueue.hpp"
#include "Datos.hpp"



using namespace std;

class AhorroViewer
{
public:
    AhorroViewer();
    virtual ~AhorroViewer();
    //int menu(Datos*);
    void listar(AhorroQueue*, Datos*);
    void listarTipo(AhorroQueue*, int);
    void listaCantidades(AhorroQueue*, int);
    //int buscar(int);
    //void mostrar(Ahorro*, Datos*);
protected:
private:
};

#endif /* AhorroViewer_hpp */
