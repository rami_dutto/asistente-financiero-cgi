//
//  queue.cpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//
#include "IngresoQueue.hpp"

IngresoQueue::IngresoQueue()
{
    qstart = qend = NULL;
}

IngresoQueue::~IngresoQueue()
{
    
}

void IngresoQueue::setQStart(IngresoNode* qstart)
{
    this->qstart = qstart;
}

IngresoNode* IngresoQueue::getQStart()
{
    return this->qstart;
}

void IngresoQueue::setQEnd(IngresoNode* qend)
{
    this->qend = qend;
}

IngresoNode* IngresoQueue::getQEnd()
{
    return this->qend;
}

void IngresoQueue::qstore(Ingreso* element)
{
    IngresoNode* newNode = new IngresoNode();
    
    newNode->setIngreso(element);
    newNode->setNext(NULL);
    
    if (qstart == NULL)
    {
        qstart = qend = newNode;
        return;
    }
    
    qend->setNext(newNode); //Hace que el último apunte al nuevo último
    
    qend = newNode; // Hace que el último sea el nuevo
}

Ingreso* IngresoQueue::qretrieve()
{
    if (this->qstart == NULL)
        return NULL;
    Ingreso* aux = this->qstart->getIngreso(); //Recupero elemento a devolver
    IngresoNode* nx = this->qstart; // Apunto nodo para no perder la referencia
    
    this->qstart = nx->getNext(); // Corro el start al siguiente nodo
    
    if (this->qstart == NULL)
        this->qend = NULL;
    
    delete nx; // Elimino el que era el primer nodo de la cola
    
    return aux; // Devuelvo el elemento recuperado
    
}

void IngresoQueue::free()
{
    while (this->qretrieve() != NULL);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */
void IngresoQueue::show()
{
    /* qstar es un puntero a un objeto de tipo node y apunta al primer nodo de la cola
     * si qstar equivale a NULL, no apunta a ningun nodo y show() termina, si no, con-
     * tinua
    if (this->qstart == NULL)
        return;
    */
    /* si se sala el 'if' se crea aux que es un puntero a un objeto de tipo node y se
     * apunta al mismo nodo que qstar, es decir; el primer nodo de la lista*/
    IngresoNode* aux = this->qstart;
    /* Mientras aux sea distinti de NULL se ejecuta el while */
    while (aux)
    {
        aux->show();
        /* Al ser aux un puntero a node, este puede invocar los metodos de node, asi que
         * invoca el metodo getElement() de node */
        cout << endl;
        /* Hago que aux apunte a la dirección de memoria del siguiente objeto node*/
        aux = aux->getNext();
    }
    
    cout << endl;
    
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
