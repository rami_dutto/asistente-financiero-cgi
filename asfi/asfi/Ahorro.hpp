//
//  Ahorro.hpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef Ahorro_hpp
#define Ahorro_hpp

#include <stdio.h>
#include <string>
#include <sstream>
#include <iomanip>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using namespace std;

class Ahorro {
public:
    Ahorro();
    ~Ahorro();
    Ahorro(sql::ResultSet* res);
    
    void setMonto(float);
    float getMonto();
    
    void setNombre(string);
    string getNombre();
    
    void fillObject(sql::ResultSet*);
    string toString(int);
    
private:
    float monto;
    string nombre;
};

#endif /* Ahorro_hpp */
