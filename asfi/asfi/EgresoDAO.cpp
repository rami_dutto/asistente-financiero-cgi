//
//  EgresoDAO.cpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "EgresoDAO.hpp"

EgresoDAO::EgresoDAO()
{
    //ctor
}

EgresoDAO::~EgresoDAO()
{
    //dtor
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
EgresoQueue* EgresoDAO::collection()
{
    /* Se crea un puntero queue que apunta a un objeto Queue*/
    EgresoQueue* egresoQueue = new EgresoQueue();
    /* MyConnection devuelve dotos los registros de la tabla ingreso y los almacena en memoria.
     * res es un puntero que apunta al comienzo de dicha dirección de memoria. */
    sql::ResultSet* res = MyConnection::instance()->query("SELECT * FROM egreso ORDER BY id");
    
    /* Se van creando objetos de tipo Ingreso a los cuales se los va seteando con los valores
     * contenidos en la memoria, el while finaliza cuando get nex ya no encuentra datos para
     * setear mas objetos de tipo Ingreso. */
    while (res->next())
        egresoQueue->qstore(new Egreso(res));
    
    delete res;
    
    cout << "<h1 class='egresos' >Egresos</h1>";
    
    return egresoQueue;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++*/

void EgresoDAO::del(Egreso* egreso)
{
    stringstream stringSQL;
    
    stringSQL << "DELETE FROM egreso WHERE id = "
    << egreso->getId()
    << ";";
    
    MyConnection::instance()->execute(stringSQL.str());
}

void EgresoDAO::save(Egreso* egreso)
{
    if (exist(egreso))
        update(egreso);
    else
        add(egreso);
}

bool EgresoDAO::exist(Egreso* egreso)
{
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM egreso WHERE id = "
    << egreso->getId()
    << ";";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    return res->next();
}

void EgresoDAO::update(Egreso* egreso)
{
    stringstream stringSQL;
    
    stringSQL << "UPDATE egreso SET "
    << "detalle ='" << egreso->getDetalle() << "', "
    << "monto = '" << egreso->getMonto() << "', "
    << "fecha = '" << egreso->getFecha() << "', "
    << "id_tipo_de_egreso = '" << egreso->getCategoria() << "', "
    << "pagado = '" << egreso->getPagado() << "'"
    << "WHERE id = "
    << egreso->getId()
    << ";";
    
    MyConnection::instance()->execute(stringSQL.str());
}

void EgresoDAO::add(Egreso* egreso)
{
    
    stringstream stringSQL;
    
    stringSQL << "INSERT INTO egreso (id_tipo_de_egreso, monto, detalle, fecha, pagado) VALUES ("
    << "'" << egreso->getCategoria() << "',"
    << egreso->getMonto() << ", "
    << "'" << egreso->getDetalle() << "', "
    << "'" << egreso->getFecha() << "', "
    << egreso->getPagado()
    << ");";
    
    MyConnection::instance()->execute(stringSQL.str());
}

Egreso* EgresoDAO::find(int id)
{
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM egreso WHERE id = "
    << id
    << ";";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    if (res->next())
    {
        Egreso* egreso = new Egreso(res);
        delete res;
        return egreso;
    }
    
    delete res;
    return new Egreso();
}

EgresoQueue* EgresoDAO::expire()
{
    EgresoQueue* egresoQueue = new EgresoQueue();
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM egreso WHERE DATEDIFF(fecha, NOW()) <= 30 AND (id_tipo_de_egreso=2 OR id_tipo_de_egreso=3) AND (pagado=0) ORDER BY fecha;";
    
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    while (res->next())
        egresoQueue->qstore(new Egreso(res));
    
    delete res;
    
    cout << "<h1 class='egresos' >Impuestos y servicios por vencer</h1>";
    
    return egresoQueue;
}

EgresoQueue* EgresoDAO::expired()
{
    EgresoQueue* egreseQueue = new EgresoQueue();
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM egreso WHERE DATEDIFF(fecha, NOW()) >= 30 AND (id_tipo_de_egreso=2 OR id_tipo_de_egreso=3) AND (pagado=0) ORDER BY fecha;";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    while (res->next())
        egreseQueue->qstore(new Egreso(res));
    
    delete res;
    
    cout << "<h1 class='egresos' >Impuestos y servicios vencidos</h1>";
    
    return egreseQueue;
}
