//
//  categoriaEgreso.hpp
//  Asistente Financiero
//
//  Created by rama on 29/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef CATEGORIAEGRESO_HPP
#define CATEGORIAEGRESO_HPP

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <string>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using namespace std;

#include "Datos.hpp"

class CategoriaEgreso {
public:
    CategoriaEgreso();
    CategoriaEgreso(sql::ResultSet*);
    ~CategoriaEgreso();
    
    void setId(int);
    int getId();
    
    void setNombre(string);
    string getNombre();
    
    void fillObject(sql::ResultSet*);
    string toString(Datos*);
    
private:
    int id;
    string nombre;
};
#endif /* categoriaEgreso_hpp */
