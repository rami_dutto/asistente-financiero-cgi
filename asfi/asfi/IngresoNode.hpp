//
//  node.hpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef INGRESONODE_HPP
#define INGRESONODE_HPP

#include <stdio.h>

#include "ingreso.hpp"

class IngresoNode
{
public:
    IngresoNode();
    IngresoNode(Ingreso*);
    virtual ~IngresoNode();
    void setIngreso(Ingreso*);
    Ingreso* getIngreso();
    void setNext(IngresoNode*);
    IngresoNode* getNext();
    void show();
protected:
    
private:
    Ingreso* ingreso;
    IngresoNode* next;
};

#endif /* node_hpp */
