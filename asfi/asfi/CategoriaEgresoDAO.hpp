//
//  CategoriaEgresoDAO.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CATEGORIAEGRESODAO_HPP
#define CATEGORIAEGRESODAO_HPP

#include <stdio.h>
#include <sstream>

#include "CategoriaEgreso.hpp"
#include "CategoriaEgresoQueue.hpp"
#include "MyConnection.hpp"


class CategoriaEgresoDAO
{
public:
    CategoriaEgresoDAO();
    virtual ~CategoriaEgresoDAO();
    
    void add(CategoriaEgreso*);
    
    void update(CategoriaEgreso*);
    
    bool exist(CategoriaEgreso*);
    
    void save(CategoriaEgreso*);
    
    void del(CategoriaEgreso*);
    
    CategoriaEgreso* find(int);
    
    CategoriaEgresoQueue* collection();
    
protected:
private:
};
#endif /* CategoriaEgresoDAO_hpp */
