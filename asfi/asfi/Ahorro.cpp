//
//  Ahorro.cpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "Ahorro.hpp"

Ahorro::Ahorro()
{
    this->monto = 0;
    this->nombre = "";
}

Ahorro::~Ahorro()
{
    // dtor
}

Ahorro::Ahorro(sql::ResultSet* res)
{
    this->fillObject(res);
}


void Ahorro::fillObject(sql::ResultSet* rs)
{
    this->setMonto(rs->getInt("suma"));
    this->setNombre(rs->getString("detalle"));
    
}


void Ahorro::setMonto(float a)
{
    this->monto = a;
}

float Ahorro::getMonto()
{
    return this->monto;
}

void Ahorro::setNombre(string a)
{
    this->nombre = a;
}

string Ahorro::getNombre()
{
    return this->nombre;
}

string Ahorro::toString(int a)
{
    stringstream monto;
    stringstream nombre;
    
    monto << setfill(' ') << left << setw(5) << this->getMonto();
    nombre << setfill(' ') << left << setw(25) << this->getNombre();
    
    if ( a == 0) {
    return "<tr><td>" + nombre.str() + "</td><td style='color:green'>$" + monto.str() + "\
    </td><td><a style='color:red;' href='./asfi.cgi?menu=4&action=2&nombre=" + nombre.str() + "'>Abandonar</a></td>\
    </td><td><a style='color:green;' href='./asfi.cgi?menu=4&action=1&nombre=" + nombre.str() + "'>Finalizar</a></td></tr>";
    } else {
        return monto.str();
    }
}
