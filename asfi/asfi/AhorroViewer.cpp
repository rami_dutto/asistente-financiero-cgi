//
//  AhorroViewer.cpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "AhorroViewer.hpp"
/* CREACION DEL CONSTRUCTOR */
AhorroViewer::AhorroViewer()
{
    //ctor
}

/* CREACION DEL DESTRUCTOR */
AhorroViewer::~AhorroViewer()
{
    //dtor
}

/*int AhorroViewer::menu(Datos* datos)
{
    AhorroViewer* ev = new AhorroViewer();
    
    cout << "Content-Type: text/html\n\r\n\r"
    <<  "<!DOCTYPE html>\n"
    <<  "<html >\n"
    <<  "<head>\n"
    <<  "  <meta charset='UTF-8'>\n"
    <<  "  <title>maquetado html5</title>\n"
    <<  "  <title>Asistente Financiero</title>\n"
    <<  "  <link type='text/css' rel='stylesheet' href='../CSS/style.css'>\n"
    <<  "  <script type='text/javascript' href='../JS/jscript.js'></script>\n"
    <<  "  <link href='https://fonts.googleapis.com/css?family=Sonsie+One' rel='stylesheet'> \n"
    <<  "  <link href='https://fonts.googleapis.com/css?family=Khand' rel='stylesheet'> \n"
    <<  "  <link href='../bootstrap/css/bootstrap.min.css' rel='stylesheet'>\n"
    <<  "  <link href='../bootstrap/css/bootstrap-theme.min.css' rel='stylesheet'>\n"
    <<  "  <link rel='stylesheet' href='css/style.css'>\n"
    <<  "</head>\n"
    <<  "\n"
    <<  "<body>\n"
    <<  "  <div class='content'>\n"
    <<  "   <div class='row'>"
    <<  "    <header>\n"
    <<  "      <a href='./asfi.cgi'><img src='https://cdn2.iconfinder.com/data/icons/money-operations/512/money_box-512.png' alt='logo'></a>\n"
    <<  "      <h1>Big $avings</h1>\n"
    <<  "      <nav>\n"
    <<  "        <ul>\n"
    <<  "          <li><a href='./asfi.cgi?menu=1'>Ingresos</a></li>\n"
    <<  "          <li><a href='./asfi.cgi?menu=3'>Egresos</a></li>\n"
    <<  "          <li><a href='./asfi.cgi?menu=5'>Supermercado</a></li>\n"
    <<  "          <li><a href='./asfi.cgi?menu=7'>Configuración</a></li>\n"
    <<  "        </ul>\n"
    <<  "      </nav>\n"
    <<  "      <article>"
    <<  "      <h2><span style='color:gray;'>Hora actual en</span><br/>Mendoza, Argentina  </h2> \n"
    <<  "      <iframe src='http://www.zeitverschiebung.net/clock-widget-iframe-v2?langua  ge=es&timezone=America%2FArgentina%2FMendoza' width='100%' height='150' frameborder='0'   se  amless></iframe> <small style='color:gray;'></small>\n"
    <<  "      </article>"
    <<  "    </header>\n"
    <<  "    <section>\n";
    
    if ((datos->getAction() == 0) || (datos->getAction() == 2)) {
        ev->listar((new AhorroDAO())->collection(), datos);
        
    } else if ((datos->getAction() == 1) || (datos->getAction() == 3)) {
        ev->mostrar((new AhorroDAO())->find(ev->buscar(datos->getId())), datos);
    }
    cout <<  "    </section>\n"
     <<  "    <aside>\n"
     <<  "    </aside>\n"
    <<  "  </div>\n"
    <<  "  </div>\n"
    <<  "</body>\n"
    <<  "</html>\n";
    
    return 6;
}
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * La función listar tiene como parametro un puntero a un objeto queue(Cola) al
 * al invocar a la funcion listar se le pasa como argumento un puntero a un bojeto queue
 * y se invoca la función show() de dicho objeto. En el archivo queue.cpp se detalla el
 * funcionamiento de la función show()*/
void AhorroViewer::listar(AhorroQueue* ahorroQueue, Datos* datos)
{
    cout <<  "<div id='constrainer'>\n"
    <<  "<div class='scrolltable'>\n"
    <<  "<table class='header'><th>Objetivo del ahorro</th><th>Cantidad ahorrada</th><th>Abandonar objetivo</th><th>Objetivo completado</th></thead></table>\n"
    <<  "<div class='body' style='height:110px;'>\n"
    <<  "<table>\n"
    <<  "<tbody>\n";
    ahorroQueue->show(0);
    cout << "</tbody>\n"
    <<  "</table>\n"
    <<  "</div>\n"
    <<  "<table>\n"
    <<  "</table>\n"
    <<  "</div>\n"
    <<  "</div>\n";
    
}

void AhorroViewer::listarTipo(AhorroQueue* ahorroQueue, int a)
{
    if (a == 0) {
        cout << "<h1 style='color:#EAB869; margin:0'>Ingresos destinados a ahorros: $ ";
    } else {
        cout << "<h1 style='color:#5A9354; margin:0'>Ingresos destinados a corrientes: $ ";
    }
    ahorroQueue->show(1);
    
    cout << "</h1>";

}

void AhorroViewer::listaCantidades(AhorroQueue* ahorroQueue, int a)
{
    if (a == 0) {
        cout << "<h1 style='color:#EAB869; margin:0'>Disponible para ahorros: $ ";
    } else {
        cout << "<h1 style='color:#5A9354; margin:0'>Disponible para corrientes: $ ";
    }
    ahorroQueue->show(1);
    
    cout << "</h1>";
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/




/*int AhorroViewer::buscar(int idIv)
{
    int id = idIv;
    
    return id;
}
Funcin para mostrar todos los valores de un Ingreso
void AhorroViewer::mostrar(Ahorro* eg, Datos* datos)
{
    CategoriaAhorroViewer* cev = new CategoriaAhorroViewer();
    string menu;
    string action;
    stringstream id;
    stringstream monto;
    stringstream detalle;
    stringstream fecha;
    stringstream pagado;
    stringstream tipo;
    
    if (datos->getAction() == 1) {
        id << eg->getId();
        monto << eg->getMonto();
        detalle << eg->getDetalle();
        fecha << eg->getFecha();
        pagado << eg->getPagado();
        action = "1";
    } else {
        id << 0;
        monto << 0;
        detalle << "";
        fecha << "";
        pagado << 0;
        action = 3;
    }
    
    
    cout <<  "<article class='add'>\n"
    <<  "<form name='form1' action='./asfi.cgi' method='get'>\n"
    <<  "<div class='categoria_add'>"
    <<  "<p>CATEGORIA</p>"
    <<  "Seleccionar: <select name='cat_id'>\n";
    
    cev->listar((new CategoriaAhorroDAO())->collection(), datos);
    // <<  "<option>1</option>\n"
    
    cout <<  "</select>\n"
    <<  "</div>\n"
    <<  ""
    <<  "<p>";
    if (pagado.str() == "0") {
        cout <<  "Pagar<label class='checkbox-inline'><input type='radio' name='pagado' value='1'> Si</label>"
        <<  "<label class='checkbox-inline'><input type='radio' name='pagado' value='0' checked> No</label><br/>";
    } else {
        cout <<  "Pagado<label class='checkbox-inline'><input type='radio' name='pagado' value='1' checked> Si</label>"
        <<  "<label class='checkbox-inline'><input type='radio' name='pagado' value='0'> No</label><br/>";
    }
    cout <<  "<input type='text' name='menu' value='3' style='display: none'>\n"
    <<  "<input type='text' name='action' value='" + action + "' style='display: none'>\n"
    <<  "<input type='text' name='confirm' value='1' style='display: none'>\n"
    <<  "<input type='text' name='id' value='" + id.str() + "' style='display: none'>\n"
    <<  "Descripción: <input type='text' name='des' value='" + detalle.str() + "' required />\n"
    <<  "<br />\n"
    <<  "Monto: <input type='text' name='monto' min='0' max='999999' value='" + monto.str() + "' required />\n"
    <<  "<br />"
    <<  "Fecha Vto.: <input type='date' value='" + fecha.str() + "' name='fecha' />\n"
    <<  "<br />\n"
    <<  "<input type='submit' value='Confirmar' /><input type='button' value='Cancelar' onClick=\"window.location='./asfi.cgi?menu=3'\">\n"
    <<  "</p>\n"
    <<  "</form>\n"
    <<  "       </article>\n";
}*/
