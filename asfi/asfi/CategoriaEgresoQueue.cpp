//
//  CategoriaEgresoQueue.cpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "CategoriaEgresoQueue.hpp"

using namespace std;

CategoriaEgresoQueue::CategoriaEgresoQueue()
{
    qstart = qend = NULL;
}

CategoriaEgresoQueue::~CategoriaEgresoQueue()
{
    
}

void CategoriaEgresoQueue::setQStart(CategoriaEgresoNode* qstart)
{
    this->qstart = qstart;
}

CategoriaEgresoNode* CategoriaEgresoQueue::getQStart()
{
    return this->qstart;
}

void CategoriaEgresoQueue::setQEnd(CategoriaEgresoNode* qend)
{
    this->qend = qend;
}

CategoriaEgresoNode* CategoriaEgresoQueue::getQEnd()
{
    return this->qend;
}

void CategoriaEgresoQueue::qstore(CategoriaEgreso* element)
{
    CategoriaEgresoNode* newNode = new CategoriaEgresoNode();
    
    newNode->setCategoriaEgreso(element);
    newNode->setNext(NULL);
    
    if (qstart == NULL)
    {
        qstart = qend = newNode;
        return;
    }
    
    qend->setNext(newNode); //Hace que el último apunte al nuevo último
    
    qend = newNode; // Hace que el último sea el nuevo
}

CategoriaEgreso* CategoriaEgresoQueue::qretrieve()
{
    if (this->qstart == NULL)
        return NULL;
    CategoriaEgreso* aux = this->qstart->getCategoriaEgreso(); //Recupero elemento a devolver
    CategoriaEgresoNode* nx = this->qstart; // Apunto nodo para no perder la referencia
    
    this->qstart = nx->getNext(); // Corro el start al siguiente nodo
    
    if (this->qstart == NULL)
        this->qend = NULL;
    
    delete nx; // Elimino el que era el primer nodo de la cola
    
    return aux; // Devuelvo el elemento recuperado
    
}

void CategoriaEgresoQueue::free()
{
    while (this->qretrieve() != NULL);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */
void CategoriaEgresoQueue::show(Datos* data)
{
    /* qstar es un puntero a un objeto de tipo node y apunta al primer nodo de la cola
     * si qstar equivale a NULL, no apunta a ningun nodo y show() termina, si no, con-
     * tinua
     if (this->qstart == NULL)
     return;
     */
    /* si se sala el 'if' se crea aux que es un puntero a un objeto de tipo node y se
     * apunta al mismo nodo que qstar, es decir; el primer nodo de la lista*/
    CategoriaEgresoNode* aux = this->qstart;
    
    /* Mientras aux sea distinti de NULL se ejecuta el while */
    while (aux)
    {
        aux->show(data);
        /* Al ser aux un puntero a node, este puede invocar los metodos de node, asi que
         * invoca el metodo getElement() de node */
        cout << endl;
        /* Hago que aux apunte a la dirección de memoria del siguiente objeto node*/
        aux = aux->getNext();
    }
    
    cout << endl;
}
