//
//  AhorroNode.hpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef AhorroNode_hpp
#define AhorroNode_hpp

#include <stdio.h>
#include <string>

#include "Ahorro.hpp"

using namespace std;

class AhorroNode
{
public:
    AhorroNode();
    AhorroNode(Ahorro*);
    virtual ~AhorroNode();
    void setAhorro(Ahorro*);
    Ahorro* getAhorro();
    void setNext(AhorroNode*);
    AhorroNode* getNext();
    void show(int);
protected:
    
private:
    Ahorro* ahorro;
    AhorroNode* next;
    
};
#endif /* AhorroNode_hpp */
