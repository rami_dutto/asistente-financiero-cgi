//
//  CategoriaIngresoViewer.hpp
//  ingresos
//
//  Created by Ramiro on 24/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CategoriaIngresoViewer_hpp
#define CategoriaIngresoViewer_hpp

#include <stdio.h>
#include <iostream>
#include <string>

#include "CategoriaIngresoController.hpp"
#include "CategoriaIngresoDAO.hpp"
#include "CategoriaIngresoQueue.hpp"
#include "Datos.hpp"



using namespace std;

class CategoriaIngresoViewer
{
public:
    CategoriaIngresoViewer();
    virtual ~CategoriaIngresoViewer();
    int menu(Datos*);
    void listar(CategoriaIngresoQueue*, Datos*);
    int buscar(int);
    void mostrar(CategoriaIngreso*, Datos*);
protected:
private:
};

#endif /* CategoriaIngresoViewer_hpp */
