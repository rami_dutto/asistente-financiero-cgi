//
//  EgresoViewer.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef EGRESOVIEWER_HPP
#define EGRESOVIEWER_HPP

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "EgresoController.hpp"
#include "EgresoDAO.hpp"
#include "EgresoQueue.hpp"
#include "Datos.hpp"



using namespace std;

class EgresoViewer
{
public:
    EgresoViewer();
    virtual ~EgresoViewer();
    int menu(Datos*);
    void listar(EgresoQueue*, Datos*);
    int buscar(int);
    void mostrar(Egreso*, Datos*);
protected:
private:
};

#endif /* EgresoViewer_hpp */
