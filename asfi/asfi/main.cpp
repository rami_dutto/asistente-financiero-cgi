//
//  main.cpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//
//#include <stdio.h>

#include <stdio.h>
#include <iostream>

/*#include <cgicc/CgiDefs.h>
#include <cgicc/Cgicc.h>
#include <cgicc/HTTPHTMLHeader.h>
#include <cgicc/HTMLClasses.h>*/

using namespace std;
//using namespace cgicc;
    
#include "MenuController.hpp"
#include "Datos.hpp"

const string OPT[ 24 ] = {
    "menu", "action", "confirm", "id",
    "cat_id", "fecha", "monto", "des",
    "nombre", "ahorro", "fijo", "pagado"
};


int main (int argc, char* const argv[]) {
    
    Datos* data = new Datos();
    
    data->setMenu(OPT[0]);
    data->setAction(OPT[1]);
    data->setConfirm(OPT[2]);
    data->setId(OPT[3]);
    data->setCatId(OPT[4]);
    data->setFecha(OPT[5]);
    data->setMonto(OPT[6]);
    data->setDescripcion(OPT[7]);
    data->setNombre(OPT[8]);
    data->setAhorro(OPT[9]);
    data->setCorriente(OPT[6], OPT[9]);
    data->setFijo(OPT[10]);
    data->setPagado(OPT[11]);
    
    /*int menu;
    cout << "Menu: ";
    cin >> menu;
    data->setMenuNoCgi(menu);*/
    /*int action;
    //int confirm;
    string nombre;*/
    
    
    /*cout << "Accion: ";
    cin >> action;
    
    //cout << "\nConfirm: ";
    //cin >> confirm;
    cout << "Nombre:";
    cin >> nombre;*/
    /*data->setActionNoCgi(action);
    //data->setConfirmNoCgi(confirm);
    data->setNombreNoCgi(nombre);
    
    
    
    cout << "Content-Type: text/html\n\r\n\r";
    cout << "accion: " << data->getAction() << endl << "<br />\n"
    <<  "id: " << data->getId() << endl << "<br />\n"
    <<  "confirm: " << data->getConfirm() << endl << "<br />\n"
    <<  "cat_id: " << data->getCatID() << endl << "<br />\n"
    <<  "fecha: " << data->getFecha() << endl << "<br />\n"
    <<  "monto: " << data->getMonto() << endl << "<br />\n"
    <<  "des: " << data->getDescripcion() << endl << "<br />\n";*/
    
    MenuController* mc = new MenuController();
    mc->menu(data);
    delete mc;
    
    return 0;
}
