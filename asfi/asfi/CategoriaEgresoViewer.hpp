//
//  CategoriaEgresoViewer.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef CategoriaEgresoViewer_hpp
#define CategoriaEgresoViewer_hpp

#include <stdio.h>
#include <iostream>
#include <string>

#include "CategoriaEgresoController.hpp"
#include "CategoriaEgresoDAO.hpp"
#include "CategoriaEgresoQueue.hpp"
#include "Datos.hpp"



using namespace std;

class CategoriaEgresoViewer
{
public:
    CategoriaEgresoViewer();
    virtual ~CategoriaEgresoViewer();
    int menu(Datos*);
    void listar(CategoriaEgresoQueue*, Datos*);
    int buscar(int);
    void mostrar(CategoriaEgreso*, Datos*);
protected:
private:
};

#endif /* CategoriaEgresoViewer_hpp */
