//
//  AhorroQueue.hpp
//  asfi
//
//  Created by Ramiro on 8/2/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef AhorroQueue_hpp
#define AhorroQueue_hpp

#include <stdio.h>
#include <iostream>

#include "AhorroNode.hpp"

using namespace std;

class AhorroQueue
{
public:
    AhorroQueue();
    ~AhorroQueue();
    void setQStart(AhorroNode*);
    AhorroNode* getQStart();
    void setQEnd(AhorroNode*);
    AhorroNode* getQEnd();
    void qstore(Ahorro*);
    Ahorro* qretrieve();
    void free();
    void show(int a);
    
protected:
    
private:
    AhorroNode* qstart;
    AhorroNode* qend;
};
#endif /* AhorroQueue_hpp */
