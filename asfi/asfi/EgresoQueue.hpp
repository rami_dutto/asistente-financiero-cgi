//
//  EgresoQueue.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef EGRESOQUEUE_HPP
#define EGRESOQUEUE_HPP

#include <stdio.h>
#include <iostream>

#include "EgresoNode.hpp"

using namespace std;

class EgresoQueue
{
public:
    EgresoQueue();
    ~EgresoQueue();
    void setQStart(EgresoNode*);
    EgresoNode* getQStart();
    void setQEnd(EgresoNode*);
    EgresoNode* getQEnd();
    void qstore(Egreso*);
    Egreso* qretrieve();
    void free();
    void show();
    
protected:
    
private:
    EgresoNode* qstart;
    EgresoNode* qend;
};

#endif /* EgresoQueue_hpp */
