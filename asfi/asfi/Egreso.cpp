//
//  egreso.cpp
//  Asistente Financiero
//
//  Created by rama on 28/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#include "egreso.hpp"

Egreso::Egreso()
{
    this->id = 0;
    this->monto = 0;
    this->detalle = "";
    this->categoria = 0;
    this->fecha = "";
    this->pagado = false;
    
}

Egreso::Egreso(sql::ResultSet* res)
{
    this->fillObject(res);
}


void Egreso::fillObject(sql::ResultSet* rs)
{
    this->setId(rs->getInt("id"));
    this->setCategoria(rs->getInt("id_tipo_de_egreso"));
    this->setFecha(rs->getString("fecha"));
    this->setMonto(rs->getInt("monto"));
    this->setDetalle(rs->getString("detalle"));
    this->setPagado(rs->getBoolean("pagado"));
    
}

/* ID. */
void Egreso::setId(int id)
{
    this->id = id;
}

int Egreso::getId()
{
    return this->id;
}

/* NOMBRE. */
void Egreso::setCategoria(int egreso)
{
    this->categoria = egreso;
}

int Egreso::getCategoria()
{
    return this->categoria;
}

/* Fecha. */
string Egreso::getFecha()
{
    return this->fecha;
}

void Egreso::setFecha(string date)
{
    this->fecha = date;
}

/* Monto.*/
int Egreso::getMonto()
{
    return this->monto;
}

void Egreso::setMonto(float total)
{
    this->monto = total;
}

/* Detalle*/
string Egreso::getDetalle()
{
    return this->detalle;
}

void Egreso::setDetalle(string detalle)
{
    this->detalle = detalle;
}

/* Pagado */
void Egreso::setPagado(int a)
{
    pagado = a;
}

int Egreso::getPagado()
{
    return this->pagado;
}


string Egreso::toString()
{
    CategoriaEgresoDAO* ced = new CategoriaEgresoDAO();
    CategoriaEgreso* ce;
    Datos* datosChild = new Datos();
    
    datosChild->setActionNoCgi(4);
    datosChild->setIdNoCgi(this->getCategoria());
    
    ce = ced->find(datosChild->getId());
    
    stringstream id;
    stringstream catid;
    stringstream catidnum;
    stringstream monto;
    stringstream detalle;
    stringstream fecha;
    stringstream pagado;
    stringstream pagadoHTML;
    stringstream urlEdit;
    stringstream urlDel;
    
    id << this->getId();
    catidnum << this->getCategoria();
    catid << setfill(' ') << left << setw(5) << ce->getNombre();
    monto << setfill(' ') << left << setw(5) << this->getMonto();
    detalle << setfill(' ') << left << setw(25) << this->getDetalle();
    fecha << setfill(' ') << left << setw(20) << this->getFecha();
    pagado << this->getPagado();
    
    if ((this->getPagado() == 0)) {
        pagadoHTML << "<td><a style='color:red' href='asfi.cgi?menu=3&action=1&id=" + id.str() + "&cat_id=" + catidnum.str() + \
        "&monto=" + monto.str() + "&des=" + detalle.str() + "&fecha=" + fecha.str() + "&pagado=1&confirm=1'>Pagar</a></td>";
    } else {
        pagadoHTML << "<td style='color:green'>Pagado</td>";
    }
    
    urlEdit << "./asfi.cgi?menu=3&action=1&id=" << this->getId();
    urlDel << "./asfi.cgi?menu=3&action=2&id=" << this->getId();
    
    return "<tr>" + pagadoHTML.str() + "<td>" + catid.str() + "</td> \
    <td style='color:red'>-$" + monto.str() + "</td><td>" + detalle.str() + "</td><td>" + fecha.str() + "</td><td> \
    <a href='" + urlEdit.str() + "'><img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></a></td><td> \
    <a href='" + urlDel.str() + "'><img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></a></td></tr>";
    
    /*
     <form method='get' action='./asfi.cgi?1&valor=" + id.str() + "'> <input type='submit' value='Modificar'/>\
     <img src='http://www.rw-designer.com/oce/res/pencil.png' alt='modificar'></form></td>\
     <td><form method='get' action='./asfi.cgi?2&valor=" + id.str() + "'> <input type='submit' value='Eliminar'/>\
     <img src='http://rhodan.com.au/plugins/slider/slideShow/delete.png' alt='eliminar'></form></td></tr>";*/
    
    /* */
}

