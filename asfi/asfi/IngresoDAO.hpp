//
//  ingresoDAO.hpp
//  Asistente Financiero
//
//  Created by rama on 27/10/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef INGRESODAO_HPP
#define INGRESODAO_HPP

#include <stdio.h>
#include <sstream>

#include "Ingreso.hpp"
#include "IngresoQueue.hpp"
#include "MyConnection.hpp"
#include "Datos.hpp"


class IngresoDAO
{
public:
    IngresoDAO();
    virtual ~IngresoDAO();
    
    void add(Ingreso*);
    
    void update(Ingreso*);
    
    bool exist(Ingreso*);
    
    void save(Ingreso*);
    
    void del(Ingreso*);
    
    void totalCorriente();
    
    Ingreso* find(int);
    
    IngresoQueue* collection();
    
protected:
private:
};

#endif /* INGRESODAO_hpp */
