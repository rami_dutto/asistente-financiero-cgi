//
//  EgresoNode.hpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#ifndef EGRESONODE_HPP
#define EGRESONODE_HPP

#include <stdio.h>

#include "Egreso.hpp"

class EgresoNode
{
public:
    EgresoNode();
    EgresoNode(Egreso*);
    virtual ~EgresoNode();
    void setEgreso(Egreso*);
    Egreso* getEgreso();
    void setNext(EgresoNode*);
    EgresoNode* getNext();
    void show();
protected:
    
private:
    Egreso* egreso;
    EgresoNode* next;
    
};
#endif /* EgresoNode_hpp */
