//
//  CategoriaEgresoDAO.cpp
//  asfi
//
//  Created by Ramiro on 25/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include "CategoriaEgresoDAO.hpp"

CategoriaEgresoDAO::CategoriaEgresoDAO()
{
    //ctor
}

CategoriaEgresoDAO::~CategoriaEgresoDAO()
{
    //dtor
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *+++++++++++++++++++++++++++HAGO FUNCIONAR ESTA PARTE PRIMERO++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
CategoriaEgresoQueue* CategoriaEgresoDAO::collection()
{
    /* Se crea un puntero queue que aounta a un objeto Queue*/
    CategoriaEgresoQueue* categoriaQueue = new CategoriaEgresoQueue();
    sql::ResultSet* res = MyConnection::instance()->query("SELECT * FROM categoria_de_egreso ORDER BY nombre;");
    
    while (res->next())
        categoriaQueue->qstore(new CategoriaEgreso(res));
    
    delete res;
    
    return categoriaQueue;
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *++++++++++++++++++++++++++++++++++++++HASTA ACA+++++++++++++++++++++++++++++++++++++++*/

void CategoriaEgresoDAO::del(CategoriaEgreso* categoriaEgreso)
{
    stringstream stringSQL;
    
    stringSQL << "DELETE FROM categoria_de_egreso WHERE id = "
    << categoriaEgreso->getId()
    << ";";
    
    MyConnection::instance()->execute(stringSQL.str());
}

void CategoriaEgresoDAO::save(CategoriaEgreso* categoriaEgreso)
{
    if (exist(categoriaEgreso))
        update(categoriaEgreso);
    else
        add(categoriaEgreso);
}

bool CategoriaEgresoDAO::exist(CategoriaEgreso* categoriaEgreso)
{
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM categoria_de_egreso WHERE id = "
    << categoriaEgreso->getId()
    << ";";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    return res->next();
}

void CategoriaEgresoDAO::update(CategoriaEgreso* categoriaEgreso)
{
    stringstream stringSQL;
    
    stringSQL << "UPDATE categoria_de_egreso SET nombre = '"
    << categoriaEgreso->getNombre()
    << "' WHERE id = "
    << categoriaEgreso->getId()
    << ";";
    
    MyConnection::instance()->execute(stringSQL.str());
}

void CategoriaEgresoDAO::add(CategoriaEgreso* categoriaEgreso)
{
    
    stringstream stringSQL;
    
    stringSQL << "INSERT INTO categoria_de_egreso (id, nombre) VALUES ("
    << categoriaEgreso->getId() << ", '"
    << categoriaEgreso->getNombre() << "'"
    << ");";
    
    MyConnection::instance()->execute(stringSQL.str());
}

CategoriaEgreso* CategoriaEgresoDAO::find(int id)
{
    stringstream stringSQL;
    
    stringSQL << "SELECT * FROM categoria_de_egreso WHERE id = "
    << id
    << ";";
    
    sql::ResultSet* res = MyConnection::instance()->query(stringSQL.str());
    
    if (res->next())
    {
        CategoriaEgreso* categoriaEgreso = new CategoriaEgreso(res);
        delete res;
        return categoriaEgreso;
    }
    
    delete res;
    return new CategoriaEgreso();
}
