//
//  menuController.hpp
//  Asistente Financiero
//
//  Created by Ramiro on 3/11/16.
//  Copyright © 2016 rama. All rights reserved.
//

#ifndef MENUCONTROLLER_HPP
#define MENUCONTROLLER_HPP

#include <stdio.h>

#include "MenuViewer.hpp"
/*#include "IngresoController.hpp"
#include "IngresoViwer.hpp"
#include "IngresoDAO"
 */

class MenuController {
public:
    MenuController();
    ~MenuController();
    void menu();
private:
    int optionMC;
};

#endif /* menuController_hpp */
