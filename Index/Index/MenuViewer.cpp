//
//  menuViewer.cpp
//  AsistenteFinanciero
//
//  Created by Ramiro on 3/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <cgicc/CgiDefs.h>
#include <cgicc/Cgicc.h>
#include <cgicc/HTTPHTMLHeader.h>
#include <cgicc/HTMLClasses.h>
#include <cgicc/CgiEnvironment.h>

using namespace std;
using namespace cgicc;

#include "menuViewer.hpp"
#include "menuController.hpp"

MenuViewer::MenuViewer()
{
    
}

MenuViewer::~MenuViewer()
{
    
}

int MenuViewer::menu()
{
    cout << "Content-type:text/html\r\n\r\n";
    cout   << "<!DOCTYPE html>\n"
    << "<html>\n"
    << "    <head>\n"
    << "        <meta charset='UTF-8'>\n"
    << "        <title>Asistente Financiero</title>\n"
    << "        <link type='text/css' rel='stylesheet' href='../CSS/style.css'>\n"
    << "        <script type='text/javascript' href='../JS/jscript.js'></script>\n"
    << "        <link href='https://fonts.googleapis.com/css?family=Sonsie+One' rel='stylesheet'> \n"
    << "        <link href='https://fonts.googleapis.com/css?family=Khand' rel='stylesheet'> \n"
    << "    </head>\n"
    << "    <body>\n"
    << "        <div id='content'>\n"
    << "            <div id='menu'>\n"
    << "                <br>\n"
    << "                <br>\n"
    << "                <br>\n"
    << "                <div >\n"
    << "                    <img id='pesos' src='../img/pesos1.png'>\n"
    << "                </div>\n"
    << "                <br>\n"
    << "                <h2 id='marca'>Big $avings</h2>\n"
    << "                </br>\n"
    << "                </br>\n"
    << "                <div class='anchor' >\n"
    << "                    <a class='selected' href='#' >HOME</a>\n"
    << "                </div>\n"
    << "                </br>\n"
    << "                <div class='anchor' >\n"
    << "                    <a id='ahorros' class='anchor1' href='./ahorros.cgi'>Ahorros</a>\n"
    << "                </div>\n"
    << "                </br>\n"
    << "                <div class='anchor' >\n"
    << "                    <a id='ingresos' class='anchor1' href='./ingresos.cgi'>Ingresos</a>\n"
    << "                </div>\n"
    << "                </br>\n"
    << "                <div class='anchor'>\n"
    << "                    <a id='egresos' class='anchor1' href='./egresos.cgi'>Gastos</a>\n"
    << "                </div>\n"
    << "                </br>\n"
    << "                <div class='anchor' >\n"
    << "                    <a id='configuracion' class='anchor1' href='./configuracion.cgi'>Configuración</a>\n"
    << "                </div>\n"
    << "                </br>\n"
    << "            </div>\n"
    << "            <div id='main1'>\n"
    << "                <h1 style='position: absolute; left: 370px; font-family: 'Sonsie One', cursive;' >Home sweet Home</h1>\n"
    << "                <img id='chancho' src='../img/chancho.jpg'>\n"
    << "            </div>\n"
    << "            <div  id='clock'> \n"
    << "                <h2><span style='color:gray;'>Hora actual en</span><br/>Mendoza, Argentina</h2> \n"
    << "                <iframe src='http://www.zeitverschiebung.net/clock-widget-iframe-v2?language=es&timezone=America%2FArgentina%2FMendoza' width='100%' height='150' framebord  er='0' seamless></iframe> <small style='color:gray;'></small> \n"
    << "            </div>\n"
    << "            <div id='contenedor'>\n"
    << "                <input id='tab-1' type='radio' name='radio-set' class='tab-selector-1' checked='checked' />\n"
    << "                <label for='tab-1' class='tab-label-1'>Reminder</label>         \n"
    << "                <input id='tab-2' type='radio' name='radio-set' class='tab-selector-2' />\n"
    << "                <label for='tab-2' class='tab-label-2'>Listas</label>                   \n"
    << "                <label for='tab-2' class='tab-label-2'>Listas</label>                   \n"
    << "                <input id='tab-3' type='radio' name='radio-set' class='tab-selector-3' />\n"
    << "                <label for='tab-3' class='tab-label-3'>%Dtos</label>                            \n"
    << "                <div class='content'>\n"
    << "                    <div class='content-1'>\n"
    << "                        <h1>Contenido1</h1>\n"
    << "                    </div>\n"
    << "                    <div class='content-2'>\n"
    << "                        <h1>Contenido2</h1>\n"
    << "                    </div>\n"
    << "                    <div class='content-3'>\n"
    << "                        <h1>Contenido3</h1>\n"
    << "                    </div>\n"
    << "                </div>\n"
    << "            </div> <!--control--!>\n"
    << "        </div>\n"
    << "    </body>\n"
    << "</html> \n";
    
    return 0;
}
