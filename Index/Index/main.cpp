//
//  main.cpp
//  Index
//
//  Created by Ramiro on 16/1/17.
//  Copyright © 2017 Ramiro. All rights reserved.
//

#include <iostream>

using namespace std;

#include "MenuController.hpp"

int main(int argc, const char * argv[]) {
    // insert code here...
    MenuController* MC = new MenuController();
    MC->menu(<#int#>);
    
    return 0;
}
